/**
 * @classdesc Objet reponse, utilisé pour répondre au client
 * @author Vincent Audergon,
 * @version 1.0
 */

/**
 * Constructeur de Response
 * @param {string} type Le type de réponse (error|warning|success|info) 
 * @param {number} code Le code HTTP de la réponse
 * @param {string} message Le message à afficher (vide par défaut)
 * @param {boolean} needconfirm Indique si le client doit attendre une confirmation de l'utilisateur avant de continuer (faux par défaut)
 * @param {object} data Les données à envoyer (vide par défaut)
 */
function Response(type, code, message, needconfirm, data) {
    this.type = type;
    this.code = code;
    this.message = message || '';
    this.needconfirm = needconfirm || false;
    this.data = data || {};
}

/**
 * Envoie la réponse
 * @param {response} res L'objet response de express
 */
Response.prototype.send = function (res) { 
    res.status(this.code).send(this);
}

module.exports = Response;