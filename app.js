/***************************************

 Main application

 Kolly Florian - EMF

  Version 0.1

 Modification:

 ****************************************/

const i18n = require('i18n');

const path = require('path');

const express = require('express');

const bodyParser = require('body-parser');

const cookieParser = require('cookie-parser');

const expressSession = require('express-session');

const expressValidator = require('express-validator');

const dotenv = require('dotenv');

dotenv.config();

const port = process.env.PORT || 55555;

const oneWeek = 604800000;

const app = express();

const config = require('./config.json');

const nocache = require('nocache');

if (process.env.local === 'true')
    global.RESTFUL = config.localRestful;
else
    global.RESTFUL = config.restful;

global.util = require('./routes/util');


app.set('view engine', 'ejs');

app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({extended: true, limit:'50mb'})); // change le body de la requête, rendant les informations disponibles sous req.body

app.use(bodyParser.json({limit:'50mb'})); // req.body est un objet JSON

app.use(expressValidator());

app.use(express.static(path.join(__dirname, 'public'), {maxAge: oneWeek}));

app.set('trust proxy', 1);

// ToDo : vérifier l'utilité de ce module (pour l'instant, pb sur Firefox et pas sur Chrome)
app.use(nocache());

const secret = config.secret;

// prépare la session pour l'utiliser avec express-session

const session = {

    secret: secret,

    resave: false,

    saveUninitialized: true

};


// Quand mis en ligne, sécurise les cookies

if (process.env.PORT) {

    session.cookie = {secure: true};

}

app.use(expressSession(session));

app.use(cookieParser(secret));


i18n.configure({

    locales: ['fr', 'de', 'it', 'en'],

    directory: path.join(__dirname, 'locales'),

    defaultLocale: 'fr',

    cookie: 'lang'

});


app.use(i18n.init);


// Envoye les requête sur le fichier de routage

app.use('/', require('./routes/routes'));


// 404 fallback

app.use((req, res, next) => {

    res.render('404');

    next();

});


app.listen(port, () => {

    console.log(`Server running on port ${port}`); // eslint-disable-line no-console

});


module.exports.app = app;

