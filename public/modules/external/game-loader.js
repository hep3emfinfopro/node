/**
 * Game Loader pour Les GRAMS (module external)
 */

let w = null;
let mode = 1;
let idStats = null;
let forcedExerciceId = null;

/**
 * Gets our iframe element.
 */
const getFrame = function () {
    return w || (w = document.getElementById('game'));
};

$(document).ready(function () {
    /**
     * Callbacks API
     */
    window.onmessage = function (e) {
        handleMessage(e.data);
    };
    wrk.send('/getJeu', true, {}, (data, success) => {
        if (success) {
            const {game} = data.data;
            const rawURL = game.script;
            let subtractedURL;
            let finalURL;
            if (forcedExerciceId) {
                subtractedURL = rawURL.slice(0, -2);
                finalURL = subtractedURL + '&code=' + forcedExerciceId + '\')';
                eval(finalURL);
            }
            // bootstrap the game
            else
                eval(rawURL);
        } else
            console.log('problème de chargement du jeu');
    });
});

/**
 * Loads a game
 **/
const loadGame = function (url) {
    const frame = document.getElementById('game');
    frame.src = url;
};

/**
 * Returns a data object.
 **/
const getMessageDataWrapper = function (type) {
    return {
        container: 'hep3-gameshub',
        type: type
    };
};

/**
 * Sends a message to the iframe.
 **/
const sendMessage = function (type, data) {
    getFrame().contentWindow.postMessage({...data, ...getMessageDataWrapper(type)}, '*');
};

/**
 * Handles a statistics submission.
 * We assume that the data type is already set to 'stats'
 **/
const handleStats = function (data) {

    /**+
     * Creates a new stats entry.
     */
    const createStatsEntry = function (mode) {
        $.ajax({
            type: 'POST',
            url: '/addStats',
            dataType: 'json',
            timeout: 2000,
            data: {
                mode: mode
            },

            success: function (xhr) {
                idStats = xhr.data.id;
            },
            error: function (xhr) {
                console.error('Create stats: Unable to create a stats entry');
            }
        });
    };

    /**+
     * Updates our current entry.
     */
    const updateStatsEntry = function (evaluation, details) {
        if (!ui_global.isParcoursMode()) return;
        console.log(details);
        if (!idStats) {
            console.warn('We don\'t have a stats ID yet.');
            return;
        }
        // Fonction pour gérer la mise à jour des stats
        const updateStats = () => {
            return $.ajax({
                type: 'POST',
                url: '/updateStats',
                dataType: 'json',
                timeout: 2000,
                data: {
                    id: idStats,
                    evaluation: evaluation
                }
            }).done(function (xhr) {
                idStats = xhr.data.id; // Met à jour l'idStats si nécessaire
            }).fail(function () {
                console.error('Update stats: Unable to update our stats entry.');
            });
        };
        // Fonction pour envoyer le tracking à LRS
        const sendTrackToLRS = () => {
            return $.ajax({
                type: 'POST',
                url: '/sendTrackToLRS',
                data: {
                    evaluation: evaluation
                }
            }).done(function (trackId) {
                console.log(trackId);
            }).fail(function () {
                console.error('SendTrack: Unable to send the track to LRS.');
            });
        };
        // Exécution parallèle des deux appels
        updateStats();
        sendTrackToLRS();
        ui_adaptiveLearning.checkAdaptiveLearning(evaluation);
        ui_parcours.checkStepDoneForStudent(evaluation);
    };

    switch (data.event) {
        case 'start':
            createStatsEntry(data.mode);
            break;
        case 'end':
            updateStatsEntry(data.evaluation, data.details);
            break;
        default:
            break;
    }
};

/**
 * Handles messages coming from the child window.
 *
 * We currently support the following operations:
 * - back   -> we should go back to the games list
 * - degree -> set the selected degree
 * - stats  -> receive stats
 */
const handleMessage = function (data) {
    if (!data.type) {
        return;
    }

    switch (data.type) {
        case 'back':
            window.location = '/jeux';
            break;

        case 'degree':
            $('#currentDegree').html(data.value);
            break;

        case 'mode': {
            const value = data.value;
            const $el = $(`#mode-${value}`);
            $('#currentMode').html($el.html());
            break;
        }

        case 'stats':
            handleStats(data);
            break;

        default:
            break;
    }
};

/**
 * Raised by the iframe when the content has been loaded.
 **/
const onloaded = (nickname, degree, forcedExoId) => {
    if (forcedExoId !== 'false') {
        forcedExerciceId = forcedExoId;
        mode = 2;
    }
    /**
     * Global layout setup
     */
    sendMessage('setup', {
        type: 'setup',
        nickname: nickname,
        degree: degree,
        mode: mode
    });
};

/**
 * Handles when the degree value changes.
 */
const onDegreeChanged = function (value) {
    sendMessage('degree', {
        degree: value
    });
};

/**
 * Handles when the mode value changes.
 */
const onModeChanged = function (value) {
    if (mode === value) {
        return;
    }
    mode = value;
    sendMessage('mode', {
        mode: value
    });
};

/**
 * Handles when the degree value changes.
 */
const check = function (value) {
    sendMessage('degree', {
        degree: value
    });
};