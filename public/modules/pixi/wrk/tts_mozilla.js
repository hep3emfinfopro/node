/**
 * @classdesc synthèse vocale via l'api Mozilla
 * @author Aous Karoui
 * @version 1.1
 */

const tts_mozilla = (function () {

    let synth, voiceSelect, textDiv, pitch, rate, playSound, stopSound, pauseSound, resumeSound, settings, rateValue,
        pitchValue, textToSpeech, selectedText, selectedParagraph, paragraphStyle;

    let voices = [];
    const frenchAcceptedVoices = ['Fabrice', 'Ariane', 'Denise', 'Henry', 'Paul', 'Julie', 'Thomas', 'Amélie'];
    const frenchUnAcceptedVoices = ['Grandma', 'Shelley', 'Hortense', 'Grandpa', 'Rocko', 'Eddy', 'Reed', 'Flo', 'Sandy', 'Jacques'];

    $(document).ready(function () {
        synth = window.speechSynthesis;

        playSound = $('#playTTS');
        stopSound = $('#stopTTS');
        settings = $('#settingsTTS');
        pauseSound = $('#pauseTTS');
        resumeSound = $('#resumeTTS');
        textDiv = $('#tutorial');

        selectedText = false;

        voiceSelect = document.querySelector('select');
        pitch = document.getElementById('pitch');
        pitchValue = document.querySelector('.pitch-value');
        rate = document.getElementById('rate');
        rateValue = document.querySelector('.rate-value');

        populateVoiceList();
        if (speechSynthesis.onvoiceschanged !== undefined)
            speechSynthesis.onvoiceschanged = populateVoiceList;

        pitch.onchange = function () {
            pitchValue.textContent = pitch.value;
        };
        rate.onchange = function () {
            rateValue.textContent = rate.value;
        };

        $(document).on('selectionchange', utterSelectedText);
    });

    /**
     * Remplit "voiceSelect" avec les voix disponibles sur le navigateur
     */
    function populateVoiceList() {
        voices = filterVoices();
        const selectedIndex =
            voiceSelect.selectedIndex < 0 ? 1 : voiceSelect.selectedIndex;
        voiceSelect.innerHTML = '';

        for (let i = 0; i < voices.length; i++) {
            const option = document.createElement('option');
            option.textContent = `${voices[i].name} (${voices[i].lang})`;
            if (voices[i].default) {
                option.textContent += ' -- DEFAULT';
            }
            option.setAttribute('data-lang', voices[i].lang);
            option.setAttribute('data-name', voices[i].name);
            voiceSelect.appendChild(option);
        }
        voiceSelect.selectedIndex = selectedIndex;
    }

    /**
     * Filtre par 2 voix à partir du tableau acceptedVoices sinon choisit 2 voix au hasard
     */
    function filterVoices() {
        const filteredVoices = synth.getVoices().filter(function (voice) {
            return (
                voice.lang.includes(currentLang) &&
                frenchAcceptedVoices.some((name, index) => voice.name.includes(name) && index < 2)
            );
        });
        if (filteredVoices.length === 0) {
            // Si acceptedVoices indisponibles, on filtre à nouveau pour obtenir 2 voix au hasard
            console.log('in filter Voices');
            voices = synth.getVoices().filter(function (voice) {
                return voice.lang.includes(currentLang) && !frenchUnAcceptedVoices.some(function (unAcceptedVoice) {
                    return voice.name.includes(unAcceptedVoice);
                });
            });
            voices = voices.slice(0, 2);
            return voices;
        } else {
            return filteredVoices;
        }
    }

    /**
     * Fonction principale de lecture
     */
    function speak(textToSpeech) {
        return new Promise((resolve, reject) => {
            let utteredText = false;
            event.preventDefault();

            toggleTTSWidgets();

            if (synth.speaking) {
                stop();
                resolve(utteredText);
                return;
            }

            if (textToSpeech !== '') {
                const utterThis = new SpeechSynthesisUtterance(textToSpeech);

                $(utterThis).on('start', function() {
                    utteredText = true;
                    ui_jeu.startRecordingDAT('playTTS');
                });

                $(utterThis).on('end', function() {
                    utteredText = false;
                    ui_jeu.stopRecordingDAT('playTTS', textToSpeech.trim());
                    stop();
                    console.log('SpeechSynthesisUtterance.onend');
                    resolve(utteredText);
                });

                utterThis.onerror = function(event) {
                    console.error('SpeechSynthesisUtterance.onerror', event);
                    reject(event);
                };

                const selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');

                for (let i = 0; i < voices.length; i++) {
                    if (voices[i].name === selectedOption) {
                        utterThis.voice = voices[i];
                        break;
                    }
                }
                utterThis.pitch = pitch.value;
                utterThis.rate = rate.value;
                synth.speak(utterThis);
            } else {
                resolve(utteredText);
            }
        });
    }

    /**
     * Affecte le texte à lire au changement de sélection
     */
    function utterSelectedText() {
        const selection = window.getSelection();
        if (selection.toString().length > 0) {
            selectedText = true;
            textToSpeech = selection.toString();
            getSelectedParagraph(selection);
        }
    }

    /**
     * Affecte le paragraphe contenant le texte à lire (pour la coloration du texte)
     @param {String} selection Le texte sélectionné pour la synthèse vocale
     */
    function getSelectedParagraph(selection) {
        selectedParagraph = $(selection.anchorNode).closest('p');
        // Si la sélection n'est pas dans une balise p, on récupère directement la div
        if (!selectedParagraph || selectedParagraph.length === 0)
            selectedParagraph = $(selection.anchorNode).closest('div');
        paragraphStyle = selectedParagraph.css('font-size');
    }

    /**
     * Colorie la partie lue du paragraphe sélectionné
     @param {String} textColor La classe css pour la mise en forme du texte à lire
     */
    function colorSelectedText(textColor) {
        try {
            // s'il s'agit de plusieurs paragraphes imbriqués, on exit sinon pb d'affichage
            if (selectedParagraph.children().length > 1)
                return;
            const texteParagraphe = selectedParagraph.text();
            const indexDebut = texteParagraphe.indexOf(textToSpeech);
            // Vérifier si la chaîne est présente dans le texte du paragraphe
            if (indexDebut !== -1) {
                // Créer une nouvelle chaîne avec la couleur spécifiée pour la partie correspondante
                const texteModifie = `<span style="${paragraphStyle}">
        ${texteParagraphe.substring(0, indexDebut)}<span class="${textColor}">${textToSpeech}</span>${texteParagraphe.substring(indexDebut + textToSpeech.length)}
    </span>`;
                // Appliquer le texte modifié au paragraphe
                selectedParagraph.html(texteModifie);
            }
        } catch (e) {
            console.error(e);
        }
    }

    /**
     * Arrête la synthèse vocale et reset les widgets
     */
    function stop() {
        event.preventDefault();
        synth.cancel();
        switchView(resumeSound, pauseSound);
        toggleTTSWidgets();
        if (selectedText)
            resetSelectedText();
    }

    /**
     * Met en pause la synthèse vocale
     */
    function pause() {
        switchView(pauseSound, resumeSound);
        synth.pause();
    }

    /**
     * Reprend la synthèse vocale
     */
    function resume() {
        switchView(resumeSound, pauseSound);
        synth.resume();
    }

    /**
     * Ré-initialise les valeurs du menu TTS
     */
    function resetMenu() {
        rate.value = util.DEFAULT_TTS_SPEED;
        rateValue.textContent = rate.value;
        pitch.value = util.DEFAULT_TTS_PITCH;
        pitchValue.textContent = pitch.value;
    }

    /**
     * Désactive le texte sélectionné
     */
    function resetSelectedText() {
        textToSpeech = textDiv.text();
        selectedText = false;
    }

    /**
     * Bascule l'affichage des widgets de synthèse vocale
     */
    function toggleTTSWidgets() {
        stopSound.toggle('fast');
        playSound.toggle('fast');
    }

    /**
     * Masque le premier élément et affiche le deuxième
     * @param {HTMLElement} nodeToHide L'élément HTML à masquer
     * @param {HTMLElement} nodeToShow L'élément HTML à afficher
     */
    function switchView(nodeToHide, nodeToShow) {
        nodeToHide.css('display', 'none');
        nodeToShow.css('display', 'inline-block');
    }

    return {
        speak,
        stop,
        pause,
        resume,
        resetMenu
    };


})
();