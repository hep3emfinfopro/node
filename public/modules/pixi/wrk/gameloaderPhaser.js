/**

 * @classdesc Chargeur de jeu

 * @author Vincent Audergon

 * @version 1.0

 */

var gameloaderPhaser = (function () {


    /**
     * Charge un jeu sur la page et le Framework
     * @param {Object} jeu Le jeu à charger
     * @param {string} langue La langue à appliquer
     * @param {number} degre Le degré harmos à appliquer
     * @param {string} gameMode Le mode reçu via le serveur (evaluate ou explore)
     * @param {number} forcedExerciceId L'id de l'exercice à forcer
     * @param {boolean} load_DAT Permet de savoir si on charge ou non les DAT
     */

    const init = function (jeu, langue, degre, gameMode, forcedExerciceId,load_DAT) {

        require(
            ['../modules/pixi/framework/Canvas', '../modules/pixi/framework/Util', '../modules/pixi/framework/Resources', '../modules/pixi/framework/proto/Log',

                '../modules/pixi/framework/ListenerManager', '../modules/pixi/framework/Gamemode', '../modules/pixi/framework/Statistics'],

            function (Canvas, Util, Resources, Log, ListenerManager, Gamemode, Statistics) {

                Gamemode = Gamemode.Gamemode;

                for (let img of jeu.lstSprites) {

                    Resources.addImage(img.nom, img.base64, img.longueur, img.hauteur, img.frames);

                }

                for (let text in jeu.texts) {

                    Resources.addText(text, JSON.parse(jeu.texts[text]));

                }

                // tous les éléments ayant une value ne sont pas considérés pour le changement de langue

                for (let input of jeu.inputs) {

                    if (input.value === '' || input.value === undefined) {

                        Resources.addInput(input.id);

                    }

                }

                Resources.setScenario(jeu.scenario);
                Resources.setEvaluation(jeu.evaluation);
                // Resources.getExerciceById(forcedExerciceId);

                // parametres à envoyer au jeu
                var forced = forcedExerciceId;
                var DAT_load = load_DAT;

                let scripts = jeu.script;

                // Util.prototype.setGamemode(Gamemode.Create);
                Util.prototype.setGamemode(gameMode);
                // lire le script du jeu
                eval(scripts);

                Resources.changeLanguage(undefined, langue);

                Resources.changeDegre(undefined, jeu.lstDegres.includes(degre) ? degre : jeu.lstDegres[jeu.lstDegres.length - 1]);

            });

    };


    return {

        init: init

    };


})();
