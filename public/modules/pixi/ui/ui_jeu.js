/**
 * @classdesc Contrôleur de la page de jeu
 * @author Vincent Audergon, Aous Karoui
 * @version 4.0
 */

var ui_jeu = (function () {

    const cssJeu = $('#styleJeu');

    const toolbox = $('#global-app-menu');

    const globalMiniAppMenu = {
        hide: $('#toolbox-hide'),
        show: $('#toolbox-show')
    };

    const inGameMiniAppMenu = {
        hide: $('#mini-app-game-menu-hide'),
        show: $('#mini-app-game-menu-show')
    };

    const gameControls = $('#gameControls');

    $(document).ready(function () {
        switchDAT({
            cssClass: 'dyslexic',
            elementsToModify: ':input, #tutorial',
            additionalCssClass: 'dyslexic-inputs',
            id: 'dyslexic'
        });
        switchDAT({
            cssClass: 'more-space-letters',
            elementsToModify: '.without-space',
            additionalCssClass: '',
            id: 'space'
        });
        switchDAT({
            cssClass: 'high-contrast',
            elementsToModify: 'canvas',
            additionalCssClass: '',
            id: 'contrast'
        });
    });

    /**
     * Initialise un élément de commutation (bouton) pour activer/désactiver une fonctionnalité additionnelle (DAT).
     * @param {object} settings - Les paramètres de configuration pour l'élément de commutation.
     */
    function switchDAT(settings) {
        let state = false;
        const switchElement = $(`#${settings.id}`);

        function toggleSwitch() {
            state = !state;
            if (state) {
                $('body *').not('i').addClass(settings.cssClass);
                $(settings.elementsToModify).addClass(settings.additionalCssClass);
                switchElement.addClass('btn-outline-dark').removeClass('btn-dark');
                startRecordingDAT(settings.id);
            } else {
                $('body *').removeClass(settings.cssClass);
                $(settings.elementsToModify).removeClass(settings.additionalCssClass);
                switchElement.removeClass('btn-outline-dark').addClass('btn-dark');
                stopRecordingDAT(settings.id);
            }
        }

        switchElement.on('click', toggleSwitch);
    }

    const toggleGlobalMiniAppMenu = () => {
        const phaserCanvas = document.getElementById('phaser-game');
        const isToolboxVisible = toolbox.is(':visible');
        // pour tracer l'activation du menu toolbox
        if (!isToolboxVisible)
            startRecordingDAT('toolbox-show');
        else
            stopRecordingDAT('toolbox-show');
        // pour masquer le canva quand il s'agit du Template Phaser
        if (phaserCanvas)
            phaserCanvas.style.display = isToolboxVisible ? 'block' : 'none';

        globalMiniAppMenu.hide.show();
        globalMiniAppMenu.show.hide();
        toolbox.toggle('fast');
        // Échange les propriétés hide et show de globalMiniAppMenu
        [globalMiniAppMenu.hide, globalMiniAppMenu.show] = [globalMiniAppMenu.show, globalMiniAppMenu.hide];
    };


    const toggleInGameMiniAppMenu = () => {
        inGameMiniAppMenu.hide.show();
        inGameMiniAppMenu.show.hide();
        $('#in-game-app-menu').toggle('fast');
        const tmp = inGameMiniAppMenu.hide;
        inGameMiniAppMenu.hide = inGameMiniAppMenu.show;
        inGameMiniAppMenu.show = tmp;
    };

    /**
     * Va chercher le jeu de la session dans la db
     */
    const loadGame = function (gameMode, forcedExerciceId) {
        wrk.send('/getJeu', true, {}, (data, success) => {
            if (success) {
                const {game, language, degre} = data.data;
                cssJeu.html(game.css);
                loadControls(game);
                updateDegre(degre);
                gameloader.init(game, language, degre, gameMode, forcedExerciceId);
            } else {
                window.location.pathname = "/jeux";
            }
        }, true);
    };

    const loadGamePhaser = function (gameMode, forcedExerciceId, DAT_load = true) {
        wrk.send('/getJeu', true, {}, (data, success) => {
            if (success) {
                const {game, language, degre} = data.data;
                cssJeu.html(game.css);
                loadControls(game);
                updateDegre(degre);
                gameloaderPhaser.init(game, language, degre, gameMode, forcedExerciceId, DAT_load);
            } else {
                window.location.pathname = "/jeux";
            }
        }, true);
    };

    /**
     * Charge les contrôles du jeu sur le côté (inputs)
     * @param {Object} jeu Le jeu dont on doit charger les contrôles
     */
    const loadControls = function (jeu) {
        for (const input of jeu.inputs) {
            let attr = '';
            let classes = '';
            // eslint-disable-next-line no-undef
            for (k in input) {
                // eslint-disable-next-line no-undef
                if (input.hasOwnProperty(k) && k !== 'class') {
                    // eslint-disable-next-line no-undef
                    attr += `${k}=${input[k]} `;
                }
            }
            if (input.class) {
                for (const classe of input.class) {
                    classes += `${classe} `;
                }
            }
            gameControls.append(`<input ${attr} class="${classes}"/>`);
        }
    };

    const updateLang = function (lang, abbr) {
        $('#currentLang').text(lang);
        // eslint-disable-next-line no-undef
        currentLang = abbr;
    };

    const updateMode = function (mode) {
        $('#currentMode').text(mode);
    };

    const updateDegre = function (deg) {
        $('#currentDegre').text(deg);
    };

    /**
     * Bascule l'affichage des fonctionnalités additionnelles DAT en fonction de l'ID donné.
     * @param {string} id - L'ID de la fonctionnalité à basculer.
     */
    const toggleGlobalMiniApp = (id) => {
        const dico = document.getElementById('dico-content');

        hideAllElementsExcept(id);
        toggleElement(id);

        if (id === 'dico-img') {
            toggleDicoContent(dico);
        } else {
            clearDicoContent(dico);
        }
    };

    /**
     * Masque tous les éléments sauf celui avec l'ID donné.
     * @param {string} id - L'ID de l'élément à afficher.
     */
    const hideAllElementsExcept = (id) => {
        for (const eltId of ['dico-img', 'my-pad', 'class-pad']) {
            if (eltId !== id) {
                const elt = $('#' + eltId);
                if (elt.data('isRunning') === true) {
                    elt.hide();
                }
            }
        }
    };

    /**
     * Bascule la visibilité de l'élément avec l'ID donné.
     * @param {string} id - L'ID de l'élément à basculer.
     */
    const toggleElement = (id) => {
        const elt = $('#' + id);
        elt.toggle();
        if (elt.is(':visible'))
            startRecordingDAT(id);
        else
            stopRecordingDAT(id);
    };


    /**
     * Commence à enregistrer le temps d'exécution de la fonction activée
     * @param {string} id L'id de l'élément activé
     */
    const startRecordingDAT = (id) => {
        // FIXME cette ligne permet de restreindre les traces au mode parcours. À enlever si on veut tracer en mode jeu libre        if (!ui_global.isParcoursMode()) return;
        const elt = $('#' + id);
        elt.data('isRunning', true);
        wrk.send('/newDATEntry', false, {
            datFunction: id
        }, function (result, success) {
            elt.data('statId', result.data.data);
            console.log('trace créée avec succès');
        }, false);
    };

    /**
     * Arrête l'enregistrement du temps d'exécution de la fonction activée
     * @param {string} id L'id de l'élément activé
     * @param {String} content Le contenu dans le cas TTS ou dico Image
     */
    const stopRecordingDAT = (id, content = '') => {
        // FIXME cette ligne permet de restreindre les traces au mode parcours. À enlever si on veut tracer en mode jeu libre
        if (!ui_global.isParcoursMode()) return;

        const elt = $('#' + id);
        elt.data('isRunning', false);
        if (!elt.data('content'))
            elt.data('content', content);

        if (elt.data('statId'))
            wrk.send('/updateDATEntry', false, {
                statId: elt.data('statId'),
                content: elt.data('content')
            }, function (data, success) {
                console.log('trace mise à jour avec succès');
            }, false);
    };

    /**
     * Bascule le contenu de l'élément dico en fonction de ses nœuds enfants.
     * @param {HTMLElement} dico - L'élément dico pour lequel basculer le contenu.
     */
    const toggleDicoContent = (dico) => {
        if (dico.hasChildNodes())
            dico.innerHTML = '';
        else
            loadDicoImage(dico);
    };

    /**
     * Efface le contenu de l'élément dico.
     * @param {HTMLElement} dico - L'élément dico pour lequel effacer le contenu.
     */
    const clearDicoContent = (dico) => {
        dico.innerHTML = '';
    };

    /**
     * Créer un pré-formulaire permettant de tracer les mots cherchés par les élèves
     * @param {HTMLElement} dico - L'élément dico pour lequel effacer le contenu.
     */
    const createTrackWordForm = (dico) => {
        const form = document.createElement('form');
        form.setAttribute('id', 'track-word');
        form.setAttribute('class', 'h-50');

        const submit = document.createElement('button');
        submit.setAttribute('type', 'submit');
        submit.setAttribute('class', 'btn btn-outline-light');
        submit.textContent = '📚 ⌕';

        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('class', 'form-control col-3');
        input.setAttribute('placeholder', 'Écrire un mot');
        input.required = true;

        const dicoContainer = document.getElementById('dico-img');
        form.appendChild(input);
        form.appendChild(submit);
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            const iframeElement = document.createElement('iframe');
            iframeElement.src = 'https://dico.apiceras.ch/?word=' + input.value;
            iframeElement.frameBorder = '0';
            dico.appendChild(iframeElement);

            $('#' + 'dico-img').data('content', input.value);
            dicoContainer.removeChild(form);
        });
        dicoContainer.appendChild(form);
    };

    /**
     * Chargement du dictionnaire dico Images via Apiceras
     */
    const loadDicoImage = (dico) => {
        const existingForm = document.getElementById('track-word');
        if (!existingForm)
            createTrackWordForm(dico);
    };

    return {
        createTrackWordForm,
        loadDicoImage,
        loadGame,
        loadGamePhaser,
        startRecordingDAT,
        stopRecordingDAT,
        toggleGlobalMiniApp,
        toggleGlobalMiniAppMenu,
        toggleInGameMiniAppMenu,
        updateLang: updateLang,
        updateMode: updateMode,
        updateDegre: updateDegre
    };

})();