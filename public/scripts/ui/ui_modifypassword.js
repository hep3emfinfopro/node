/**
 * @classdesc Contrôleur de la page de modification de mot de passe
 * @author Florian Kolly, Vincent Audergon
 * @version 2.0 
 */
var ui_modifypassword = (function () {

    const inputPassword = $('input[name=password]');
    const inputVerifyPassword = $('input[name=confirm]');
    const lblPasswordNotSame = $('#password-not-same');
    const lblSecurity = $('#password-not-secured');
    const btnModify = $('#btnModify');

    $(document).ready(function () {
        inputPassword.on('change input', _testPassword);
        inputVerifyPassword.on('change input', _testPassword);
    });

    /**
     * Envoie le formulaire de changement de mot de passe au serveur
     */
    const sendPassword = function () {
        let password = inputPassword.val();
        let confirm = inputVerifyPassword.val();
        wrk.send('/modifyPassword', true, { password: password, confirm: confirm }, function (data, success) {
            if (success) window.location.pathname = "/";
        });
    }

    /**
     * Vérifie les mots de passes, si c'est OK débloque le bouton de validation
     */
    const _testPassword = function () {
        let ok = true;
        if (inputPassword.val() !== inputVerifyPassword.val()) {
            lblPasswordNotSame.show();
            ok = false;
        } else {
            lblPasswordNotSame.hide();
        }
        if (ui_global.regexPassword.test(inputPassword.val())) {
            lblSecurity.hide();
        } else {
            lblSecurity.show();
            ok = false;
        }
        btnModify.prop('disabled', !ok);
    }

    return {
        sendPassword: sendPassword
    }

})();
