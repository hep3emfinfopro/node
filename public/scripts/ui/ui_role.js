/**

 * @classdesc Contrôleur de la page des rôles

 * @author Sturzenegger Erwan

 * @version 1.0

 */

var ui_role = (function () {

    $(document).ready(function () {

    });

    const updateRole = function (userId, role) {
        var newRole = $('#admin' + userId).prop('checked') ? 3 : $('#validateur' + userId).prop('checked') ? 2 : 1;
        wrk.send('/updateRole', false, {userId: userId, role: newRole}, function (data, success) {
            $('#role' + userId).text(data.message.role);
        }, false);

        switch (newRole) {
            case 1 :
                $('#admin' + userId).prop('checked', false);
                $('#validateur' + userId).prop('disabled', false);
                $('#validateur' + userId).prop('checked', false);
                break;
            case 2:
                $('#admin' + userId).prop('checked', false);
                $('#validateur' + userId).prop('disabled', false);
                $('#validateur' + userId).prop('checked', true);
                break;
            case 3 :
                $('#admin' + userId).prop('checked', true);
                $('#validateur' + userId).prop('disabled', true);
                $('#validateur' + userId).prop('checked', true);
                break;
            default :
                break;
        }

    };

    return {
        updateRole
    };

})();
