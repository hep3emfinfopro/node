/**

 * @classdesc Contrôleur de la page de validation

 * @author Sturzenegger Erwan

 * @version 1.0

 */

var ui_valider = (function () {

    var degre = null;
    var classe = null;
    var game = null;
    var ex = {};

    var storage = undefined;

    $(document).ready(function () {
        storage = sessionStorage;
        $('#degre').change(function () {
            var value = $('#degre').val();
            if (value !== 'null')
                degre = value;
            else
                degre = null;
            _filter();
        });
        $('#classe').change(function () {
            var value = $('#classe').val();
            if (value !== 'null')
                classe = value;
            else
                classe = null;
            _filter();
        });
        $('#games').change(function () {
            var value = $('#games').val();
            if (value !== 'null')
                game = value;
            else
                game = null;
            _filter();
        });

        if (storage.getItem('valider')) {
            var pref = storage.getItem('valider');
            pref = JSON.parse(pref);
            degre = pref.degre;
            game = pref.game;
            classe = pref.classe;
            $('#degre').val(degre);
            $('#classe').val(classe);
            $('#games').val(game);
            _filter();
        }
    });

    function _filter() {
        var pref = {
            classe: classe,
            degre: degre,
            game: game
        };
        storage.setItem('valider', JSON.stringify(pref));

        const list = $('.exercice');
        list.removeClass('hide');
        if (degre) list.not('.degre' + degre).addClass('hide');
        if (classe) list.not('.classe' + classe).addClass('hide');
        if (game) list.not('.game' + game).addClass('hide');
        var hasContent = false;
        for (let comp of list) {
            if (!$(comp).hasClass('hide')) {
                hasContent = true;
                break;
            }
        }
        if (!hasContent)
            $('#noRow').show();
        else
            $('#noRow').hide();
    }

    const _delete = function (id) {
        wrk.send('/admin/valider/supprimer', false, {exercice: id}, function (response, success) {
            location.reload(true);
        }, false);
    };

    return {
        delete: _delete
    };
})();

