/**
 * @classdesc Contrôleur de la page de validation

 * @author Huwiler Paul

 * @version 1.0
*/
let arraySTM = [];
let sprites = {};
let exId = 0;
let currentExercices;
const sizeSprite = 38;
const sizeCanvas = 600
//variable de correction


function deleteExercices() {

    wrk.send('/admin/valider/supprimer', false, { exercice: exId }, function (response, success) {
        console.log("response")
        console.log(response)
        //TODO:
        if (success) {
            Swal.fire({
                title: "Suppression réussi",
                icon: "success",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK'
            }).then((result) => {
                $('#returnBtn').click();
            });
        } else {
            Swal.fire({
                title: "echec de la suppression",
                icon: "error",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK'
            })
        }

    }, true);
}

function SaveAndExit() {
    wrk.send('/updateExercice', false, { exercice: JSON.stringify(currentExercices), exId: exId }, function (data, success) {
        if (success) {
            wrk.send('/updateExerciceState', false, { state: 2, exId: exId }, function (data) {
                Swal.fire({
                    title: data.message,
                    icon: data.type,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then(() => {
                    $('#returnBtn').click();
                });
            }, true);
        }
    }, true);
}

function showExercices(exe, id) {
    console.log(exe)
    exId = id;
    currentExercices = exe.exercice;
    //création du canvas
    let canvas = document.createElement('canvas');
    canvas.id = "c";
    canvas.width = sizeCanvas;
    canvas.height = sizeCanvas;
    //canvas.style.zIndex = 8;
    //canvas.style.position = "absolute";

    //canvas.style.border = "1px solid";
    $(".canvas").append(canvas);
    //  document.body.appendChild(canvas);
    wrk.send('/getSprites', false, { gameId: 11 }, function (data, success) {
        fillSprites(data)
        const correctionXCal = (sizeCanvas - currentExercices.difficulty.x) / 2;
        const correctionYCal = currentExercices.difficulty.locationy - (currentExercices.difficulty.y / 2);
        console.log(correctionXCal)
        //récupération du canvas
        canvas = document.getElementById("c");
        let ctx = canvas.getContext("2d");
        console.log(canvas)
        //ajout de la grille
        let calendare = new Image();
        calendare.onload = function () {
            //console.log("les coordonnées de la grille \nX ->" + exe.difficulty.locationx + "\nY ->" + exe.difficulty.locationy)
            //le x et y de la grille
            ctx.drawImage(calendare, 0, 0, currentExercices.difficulty.x, currentExercices.difficulty.y, correctionXCal, correctionYCal, currentExercices.difficulty.x, currentExercices.difficulty.y);
            placeSprite(currentExercices.elementsInTab.sprites, ctx);
            placeSTM(currentExercices.spriteToMove, ctx);
        };
        //calendare.src = sprites[exe.difficulty.name]
        //ctx.drawImage(calendare, exe.difficulty.locationx, exe.difficulty.locationy);
        calendare.src = `data:image/png;base64,` + sprites[currentExercices.difficulty.name];
        //ctx.drawImage(calendare, 10, 10);
    });

}
function placeSTM(stm, ctx) {
    let count = 1
    for (let s in stm) {
        if (stm[count] != undefined) {
            console.log(stm[count])
            let currentStm = stm[count];
            let dateSTM = giveDateSTM(currentStm);
            let data = {
                sprite: currentStm,
                date: dateSTM
            }
            let spriteCorrectionX = currentStm.verif.x - (sizeSprite / 2);
            let spriteCorrectionY = currentStm.verif.y - (sizeSprite / 2);
            arraySTM.push(data)
            let sp = new Image();
            sp.onload = function () {
                ctx.drawImage(sp, spriteCorrectionX, spriteCorrectionY + 3)
            }
            sp.src = `data:image/png;base64,` + sprites[currentStm.sprite]
            //let imgHtml = `<img src = "data:image/png;base64,` + sp.src + `" id="imagestm` + count - 1 + `">`
            let id = count - 1;
            $(".list-img").append(`<img src = "data:image/png;base64,` + sprites[currentStm.sprite] + `" id=` + id + ` onclick ="creatPopUp(this)">`);
            count++;
        } else {
            break;
        }
    }
}

function giveDateSTM(currentStm) {
    //let graphics = new PIXI.Graphics();
    console.log(currentStm)
    let x = currentStm.verif.x
    let y = currentStm.verif.y
    let dateSTM;
    let count = 1;
    for (let d in currentExercices.elementsInTab.magnets) {
        let m = currentExercices.elementsInTab.magnets[count]
        if (m.x == x && m.y == y) {
            dateSTM = m.text;
            break;
        }
        count++;
    }
    return dateSTM;
}
/**
 *
 * @param {<img>} e
 */
function creatPopUp(e) {
    let htmlToInsertInPopup
    let spriteimg = arraySTM[e.id]
    console.log("start")
    console.log(arraySTM)
    if (spriteimg) {
        htmlToInsertInPopup = `<div> <div>` + spriteimg.date + `<div><br> `;
        htmlToInsertInPopup += `<div id="all">
        <div id = "top-pop-pu">
        <label for="fname">Phrase d'indice</label><br>
        <input type="text" id="mainSentence" name="fname"  value="`+ spriteimg.sprite.text + `" ><br>`;
        htmlToInsertInPopup += `<div style="text-align: left; margin-top: 5px" class="position-relative"><br>Ecrire les 4 phrases de justifications possible et s\u00e9lectionner la phrase correcte<br>`
        for (let i = 0; i < 4; i++) {
            if (spriteimg.sprite.popup.answers[i + 1].ok) {
                htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1" checked>`
            } else {
                htmlToInsertInPopup += `<input type="radio" class="radiobtn"id="input_btn_rd` + i + `" name="validate_" value="op1">`
            }
            htmlToInsertInPopup += `<input type="text" id="input_txt_` + i + `" name="fname" value="` + spriteimg.sprite.popup.answers[i + 1].text + `"> <br><br>`
        }
        Swal.fire({
            confirmButtonText: 'Validation',
            showCancelButton: true,
            showConfirmButton: true,
            html:
                htmlToInsertInPopup + `</div></div>`

        }).then((result) => {
            spriteimg.sprite.text = $("#mainSentence").val()
            for (let i = 0; i < 4; i++) {
                //TODO:->modification
                spriteimg.sprite.popup.answers[i + 1].text = $('#input_txt_' + i).val(),
                    spriteimg.sprite.popup.answers[i + 1].ok = $('#input_btn_rd' + i).is(':checked')
            }
            arraySTM[e.id] = spriteimg;
            let count = 1;
            for (let key in currentExercices.sprite) {
                if (currentExercices.sprite[count]) {
                    let x = currentExercices.sprite[count].verif.x;
                    let y = currentExercices.sprite[count].verif.y;
                    if (x == spriteimg.verif.x && y == spriteimg.verif.y) {
                        currentExercices.sprite[count] = spriteimg
                    }
                }
            }
        })
    }
    //console.log(currentExercices)
}
function placeSprite(spriteInTab, ctx) {
    console.log("placeSTM")
    console.log(spriteInTab)
    let count = 1
    for (let s in spriteInTab) {
        if (spriteInTab[count] != undefined) {
            //sprite courant
            let currentStm = spriteInTab[count];
            console.log(currentStm)
            let spriteCorrectionX = currentStm.x - (sizeSprite / 2);
            let spriteCorrectionY = currentStm.y - (sizeSprite / 2);
            let sp = new Image();
            sp.onload = function () {
                ctx.drawImage(sp, 0, 0, sizeSprite, sizeSprite, spriteCorrectionX, spriteCorrectionY + 3, sizeSprite, sizeSprite)
            }
            sp.src = `data:image/png;base64,` + sprites[currentStm.nomFichier]

            count++;
        } else {
            break;
        }
    }
}




function fillSprites(data) {
    data = JSON.parse(data);
    for (let sprite of data) {
        ///var html = '<img src="data:image/png;base64,' + sprite.base64 + '" height="70" width="25" style="margin-bottom: 20px">';
        let html = sprite.base64;
        sprites[sprite.nom] = html;
    }
    JSON.stringify(sprites)
    console.log(sprites)
}

function placebtn(){
    let count = 1;
    for (let key in currentExercices.spriteToMove) {
        if (currentExercices.spriteToMove[count]) {
            let btn = document.createElement('input');
            btn.type = "button";
            btn.style.background = "none";
            btn.style.border = "none";
            btn.onclick = "changeMenu('howToPlayDiv', 'mouseclick')";

        }
    }
}

/*
function creatCalendare(numWeek) {
    let htmlTab = `<table>`;
    let interWeek = `<tr><td id = "interWeek" colspan="8"></td></tr>`;
    if (numWeek == 0) {

    } else if (numWeek == 1) {
        numDayInMonth = 13
    } else {
        for (let i = 0; i < numWeek; i++) {
            htmlTab += creatWeek();
            htmlTab += interWeek;
        }

    }
    htmlTab += `</table>`;
    //console.log(htmlTab)
    $('.calendarHtml').append(htmlTab)
}

function creatWeek() {
    let htmlTab = ``;
    let firstLine = true;
    let numDayInMonth = 6;
    let indexTimeInDay = 0;
    let indexMagnet = 0
    let indexMagnet2 = 0

    for (let i = 0; i < 4; i++) {
        htmlTab += `<tr>`

        for (let i = 0; i < 8; i++) {
            //si c'est la première ligne
            if (firstLine) {
                htmlTab += `<td>`
                htmlTab += days[i]
            } else {
                //si c'est la première case de la ligne
                if (i == 0) {
                    htmlTab += `<td>`
                    //C'est une case donnant le moment de la journée
                    htmlTab += timeInday[indexTimeInDay]
                    indexTimeInDay++;
                    if (indexTimeInDay > 2) { indexTimeInDay = 0 }
                } else {
                    //C'est une case vide
                    if (exe.elementsInTab.magnets[indexMagnet]) {
                        //console.log(exe.elementsInTab.magnets[indexMagnet])
                        htmlTab += `<td id="` + exe.elementsInTab.magnets[indexMagnet].text + `">`
                    }
                    htmlTab += `</td>`

                    indexMagnet = indexMagnet + 3
                }
            }
            htmlTab += `</td>`

            //htmlTab += `</td>`

        }
        indexMagnet = indexMagnet2 ++;
        console.log("indexMagnet -> " +indexMagnet)
        firstLine = false
        htmlTab += `</tr>`
    }
    //}

    console.log(htmlTab)
    return htmlTab;
    //
}

function placeImage(){

    let count = 1;
    //on parcourt les magnets
    for(let key in exe.elementsInTab.magnets){
       let m = exe.elementsInTab.magnets[count];
       if(m){
           let countMagnets = 1;
           //on parcourt les stm
        for(let keyMagnets in exe.spriteToMove){
            let s = exe.spriteToMove[countMagnets];
            if(s){
                //si le x et y sont égale on prend le txt de l'aimant et on s'en sert pour identifier la cell.
                if(s.verif.x == m.x && s.verif.y == m.y){
                    $("#"+m.text).append(`<img src="`+sprites[s.sprite]+`">`);
                    break
                }
            }
        }
       }
       count++;
    }

    console.log("YEEEEEEEEEEEEEEEEET")
    console.log( $(".list-img"));
}
*/
/*

console.log("sdfuihgsdui")
let ui_modifier_exercice_4 = (function () {

    const saveAndExit = function () {
        console.log("test2")
    };
    return {
        //buildTree,
        //displayInfos,
        saveAndExit
        //delete: _delete
    };
})();*/
