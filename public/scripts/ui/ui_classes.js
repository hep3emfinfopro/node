/**

 * @classdesc Contrôleur de la page de sélection des classes et des élèves

 * @author Florian Kolly, Vincent Audergon

 * @version 2.0

 */

var ui_classes = (function () { // eslint-disable-line no-unused-vars


    //Others

    const modal_share = $('#modal_share');

    const progressbar = $('#progressbar');

    const tooltip = $('[data-toggle="tooltip"]');

    //Lists

    const lstEleves = $('#lstEleves');
    const classList = $('#classList');

    //Buttons

    const btnToggleAddEleve = $('#addEleve');

    const btnAddEleve = $('#inputAddEleve');
    const btnParcoursEleve = $('#parcoursEleve');
    const btnParcoursClasse = $('#parcoursClasse');

    const btnJouer = $('#jouer');

    const btnAddClasse = $('#inputAddClass');

    const btnProfilClasse = $('#profilClasse');

    const btnProfilEleve = $('#profilEleve');

    const btnSend = $('#sendbtn');

    //Inputs

    const inputText = $('textarea#text');

    const inputEmail = $('input#email');

    let inputEleveName = undefined; //Pas encore existant

    let inputAddEleve = undefined;

    //Labels

    const lblLink = $('#modal_share_link');

    const lblShare = $('#modal_share_text');


    let selectedClasse = -1;


    $(document).ready(function () {

        tooltip.tooltip();

        progressbar.hide();

        $('.eleve').removeClass('selected');

        $('.eleve').hide();

        btnProfilEleve.hide();

        btnToggleAddEleve.hide();
        btnParcoursEleve.hide();
        btnParcoursClasse.hide();

        btnJouer.hide();

        btnAddEleve.hide();

        btnAddClasse.hide();

        btnProfilClasse.hide();

    });

    /**
     * Affiche la liste des classes pour un enseignant
     * @param {string} classes
     * @param {string} URL l'URL de la plateforme (peut varier entre test et prod)
     * @param {string} teacherID l'id de l'enseignant
     * @param {string} token le token de la session
     */
    const showClasses = function (classes, URL, teacherID, token) {
        JSON.parse(classes).forEach((classe) => {
            const classHtml = `
            <a href="#" name="classe"
               class="list-group-item classe"
               id="classe${classe.id}"
               onclick="ui_classes.setClasse(${classe.id});">
                <span>${classe.nom} (${classe.degre})</span>          
                <span class="float-right">
                    <i onClick="ui_classes.openQrCodeModal('${classe.id}','${URL}','${teacherID}','${token}');"
                       class="fa fa-qrcode mr-4"
                       title = "Connecter mes élèves"
                       data-toggle="modal" data-target="#qrCodeModal"></i>
                    <i onClick="ui_classes.openSharingModal('${classe.uuid}');"
                       class="fas fa-share-square"
                       title="Partager la classe"></i>
                </span>
            </a>`;
            classList.append(classHtml);
        });
    };

    /**
     * Affiche une classe et la liste d'élèves lors d'un accès via qr-code
     * @param {string} classe
     */
    const showClassQrCode = function (classe) {
        const parsedClass = JSON.parse(classe);
        const classHtml = `
            <a href="#" name="classe"
               class="list-group-item classe"
               id="classe${parsedClass.id}"
               onclick="ui_classes.setClasse(${parsedClass.id});">
                <span>${parsedClass.nom} (${parsedClass.degre})</span>                
            </a>`;
        classList.append(classHtml);
        setClasse(parsedClass.id);
    };

    /**
     * Affiche la liste des classes avec les options Adaptive Learning
     * @param {object} classes
     * @param {number} teacherId
     */
    const showClassesAdaptiveLearning = function (classes, teacherId) {
        JSON.parse(classes).forEach((classe) => {
            const classHtml = `
            <a href="#" name="classe"
               class="list-group-item classe"
               id="classe${classe.id}"
               onclick="ui_classes.setClasse(${classe.id});">
                <span>${classe.nom} (${classe.degre})                  
                    <input type="button" name="" class="btn btn-outline-warning float-right"
                         title="Rattacher à Profile Engine" value="🔗"
                         onclick="ui_classes.connectClassToProfileEngine(291,'${classe.id}','${classe.nom}')">
                    <input type="button" name=""
                         class="btn btn-outline-warning float-right"
                         title="Accéder à Profile Engine" value="🌐"
                         onclick="ui_classes.openProfileEngine(291,'${classe.id}', '${teacherId}')">
                </span>
            </a>`;
            classList.append(classHtml);
        });
    };

    /**
     * Génère un qr-code avec l'id d'une classe
     * @param {number} classId l'id de la classe sélectionnée
     * @param {string} URL l'URL de la plateforme (peut varier entre test et prod)
     * @param {string} teacherID l'id de l'enseignant
     * @param {string} token le token de la session
     */
    const openQrCodeModal = function (classId, URL, teacherID, token) {
        var qr = new QRious({
            element: document.getElementById('qrcode'),
            size: 256,
            value: `${URL}/qrCodeAccess/${classId}/${teacherID}`
        });
        document.getElementById('connexionCode').innerHTML = `${classId}-${teacherID}`;
    };

    /**
     * Ouvre la page de connexion d'un élève
     */
    const connectStudent = function () {
        const studentInput = document.getElementById('studentCode').value;
        const studentInputs = studentInput.split('-');
        if (studentInputs.length === 2) {
            const classId = studentInputs[0];
            const teacherID = studentInputs[1];
            window.location = `/qrCodeAccess/${classId}/${teacherID}/`;
        } else {
            console.error('Le format de la chaîne n\'est pas correct.');
        }
    };


    /**
     * Affiche la liste des élèves sur la page
     * @param {array} eleves La liste des élèves
     */
    const afficheEleves = function (eleves) {
        let checkbox = ``;

        btnProfilEleve.hide();
        btnProfilClasse.show();
        btnJouer.hide();
        btnAddEleve.show();
        lstEleves.html('');
        for (const eleve of eleves) {
            // Si on est sur la page des parcours on ajoute le checkbox
            if (window.location.href.indexOf('designParcours') > -1)
                checkbox = `<input type="checkbox" onchange="ui_parcours.assignParcours(this,${eleve.id},'${eleve.nom}','student')">`;
            lstEleves.append(`<a href="#" name="student" class="d-flex justify-content-between list-group-item eleve eleve${eleve.id}" id="eleve${eleve.id}" onclick="ui_classes.setEleve(${eleve.id},'${eleve.nom}');">${eleve.nom} ${checkbox}</a>`);
        }
    };

    /**
     * Affiche/masque le formulaire pour créer un nouvel élève
     */
    const toggleAddEleve = function () {
        if (inputAddEleve !== undefined && inputAddEleve.length > 0) {
            inputAddEleve.remove();
            inputEleveName = undefined;
            inputAddEleve = undefined;
        } else {
            lstEleves.append(`<a href="#" class="list-group-item" id="inputAddEleve">
        <div class="input-group">
          <input type="text" name="eleveName" id="eleveName" class="form-control form-control-sm" required>
          <span class="input-group-append">
            <input type="button" onclick="ui_classes.addEleve()" value="+" class="btn btn-primary btn-sm">
          </span>
        </div>
      </a>`);
            inputEleveName = $('input#eleveName');
            inputAddEleve = $('#inputAddEleve');
            ui_global.addEnterListener(inputEleveName, addEleve);
        }
    };

    /**
     * Sélectionne un élève et le montre sur la page
     * @param {number} id
     */
    const chooseEleve = function (id) {
        $('.eleve').removeClass('selected');
        $('#eleve' + id).addClass('selected');
        btnJouer.show();
        btnProfilEleve.show();
    };

    /**
     * Sélectionne une classe et l'affiche sur la page
     * @param {number} id L'id de la classe
     */
    const setClasse = function (id) {
        $('.classe').removeClass('selected');
        $('#classe' + id).addClass('selected');
        selectedClasse = id;
        btnToggleAddEleve.show();
        btnParcoursClasse.show();
        btnParcoursEleve.hide();

        wrk.send('/setClasse', false, {classeid: id}, function (data, success) {
            if (success) {
                wrk.send('/eleves', false, {idclasse: selectedClasse}, function (data, success) {
                    if (success) {
                        afficheEleves(data.data.eleves.data);
                        if (window.location.href.indexOf('designParcours') > -1)
                            // On appelle cette fonction uniquement sur la page designParcours
                            ui_parcours.loadCheckBoxForStudents();
                    }
                }, false);
            }
        }, false);
    };


    /**
     * Définit l'élève dans la session
     * @param {number} id l'identifiant de l'élève
     * @param {string} nom le nom de l'élève
     */
    const setEleve = function (id, nom) {
        btnParcoursClasse.hide();
        btnParcoursEleve.show();
        chooseEleve(id);
        wrk.send('/setEleve', false, {eleveid: id, classeid: selectedClasse}, function (data, success) {
            if (!success) {
                btnJouer.hide();
                btnProfilEleve.hide();
            }
            if (document.body.dataset.page === 'qr-code-access') {
                ui_global.showConfirmAlert('info', null, `Se connecter avec le compte de ${nom} ?`, `Connexion : ${nom}`, function () {
                    window.location.href = '/parcoursEleve/' + id;
                });
            }
        }, false);
    };

    /**
     * Ajoute un élève selon les informations données dans le formulaire de création d'élève
     */
    const addEleve = function () {
        if (selectedClasse != -1) {
            let eleveName = inputEleveName.val();
            wrk.send('/addEleve', true, {eleveName: eleveName, classe: selectedClasse}, function (data, success) {
                if (success) {
                    setClasse(selectedClasse);
                }
            }, false);
        }
    }

    //============================
    //          Partage
    //============================

    /**
     * Ouvre le formulaire de partage de classe
     * @param {string} uuid L'uuid du lien de partage
     */
    const openSharingModal = function (uuid) {
        const link = `${location.protocol}//${location.hostname}/share/${uuid}`;
        lblLink.text(link);
        lblShare.text(link);
        lblLink.text(link);
        modal_share.modal('show');
    };

    /**
     * Envoie l'email de partage selon les informations sur formulaire
     */
    const sendEmail = function () {
        let link = lblLink.text();
        let text = inputText.val();
        let email = inputEmail.val();
        btnSend.hide();
        progressbar.show();
        wrk.send('/sendSharingMail', true, {email: email, text: text, link: link}, (data) => {
            progressbar.hide();
            modal_share.modal('hide');
            btnSend.show();
        });
    };

    //============================
    //     Adaptive Learning
    //============================
    /**
     * Rattache une classe de GamesHub à un référentiel sur Profile Engine. Techniquement, consiste à créer un groupe (une classe) d'étudiants sur Profile Engine (projet Comper) pour un référentiel donné
     * @param fwkId : number  Id du Framework (référentiel) auquel la classe sera rattachée
     * @param classId : number   Id de la classe
     * @param className : string Nom de la classe
     */
    const connectClassToProfileEngine = function (fwkId, classId, className) {
        const studentsList = [];
        // Timeout pour être sûr que la liste des élèves a été mise à jour au niveau du DOM
        setTimeout(function () {
            const DOM_studentsList = document.querySelectorAll('#lstEleves a');
            DOM_studentsList.forEach(function (element) {
                const student = {
                    id: element.id, name: element.textContent
                };
                studentsList.push(student);
            });
            wrk.send('/connectClassToProfileEngine', true, {
                fwkId: fwkId, classId: classId, className: className, studentsList: JSON.stringify(studentsList)
            }, (response, success) => {
                if (success) {
                    console.log(response);
                }
            });
        }, 1000);
    };

    /**
     * Ouvre, dans un nouvel onglet, le groupe COMPER correspondant à la classe sélectionnée.
     *
     * Cette fonction sollicite un jeton JWT qu'elle injecte dans un formulaire créé à la volée. Les données de ce
     * formulaire sont ensuite envoyées au serveur COMPER via une requête POST. Les données requises sont :
     * - token, le jeton JWT
     * - fromPlatform, le nom de la plateforme
     *
     * @param fwkId l'identifiant du référentiel
     * @param classId l'identifiant de la classe
     * @param teacherId l'identifiant de l'enseignant
     */
    const openProfileEngine = function (fwkId, classId, teacherId) {
        wrk.send('/openProfileEngine', true, {
            fwkId: fwkId, classId: classId, teacherId: teacherId,
        }, (response, success) => {
            if (success) {
                const form = document.createElement("form");
                form.target = "_blank";
                form.method = "POST";
                form.action = "https://compervm-dev.liris.cnrs.fr/cpe/";
                const fromPlatform = document.createElement('input');
                fromPlatform.type = "hidden";
                fromPlatform.name = "fromPlatform";
                fromPlatform.value = "gameshub";
                form.appendChild(fromPlatform);
                const token = document.createElement('input');
                token.type = "hidden";
                token.name = "token";
                token.value = response.token;
                console.log(response.token);
                form.appendChild(token);
                document.body.appendChild(form);
                form.submit();
            }
        });
    };

    const createModal = function (stepsJson, stepNumber, id, consigne) {
        let script = '';
        let idToSave = id;
        if (id !== 2) idToSave = id + 1;
        // On vérifie que le modal n'existe pas déjà
        if (!document.getElementById('modal' + id)) {
            let stringifiedJson = '';
            if (stepsJson !== null) {
                stringifiedJson = JSON.stringify(stepsJson);
            }
            let modalContent = '<input type="text" className="form-control form-control-lg mt-5 mb-5" placeholder="Écrire ici la consigne de l\'activité debranchée" id="inputConsigne' + id + '" value="' + consigne + '">';
            let modalFooter = '<div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button><button type="button" id="editStepJson' + id + '" class="btn btn-primary" onclick="ui_parcours.saveConsigne(' + idToSave + ')" data-dismiss="modal">Valider</button></div>';
            if (window.location.href.indexOf('runParcours') > -1) {
                script = '<script>document.getElementById("editStepJson' + id + '").addEventListener("click",function(){ui_parcours.editStepStatusInJson(' + stringifiedJson + ',' + stepNumber + ',1);})</script>';
                if (consigne.includes('www') || consigne.includes('http')) modalContent = '<iframe id="iframe" src="' + consigne + '" height="700px" width="700"><a href="/' + consigne + '" target="_blank"></a></iframe>'; else modalContent = consigne;
                modalFooter = '<div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button><button type="button" id="editStepJson' + id + '" class="btn btn-primary" data-dismiss="modal">Activité validée avec l\'enseignant</button></div>';
            }
            $('body').append('<div class="modal fade" id="modal' + id + '" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">\n                                   <div class="modal-dialog">\n                                     <div class="modal-content" style="width: 800px;">\n                                       <div class="modal-header">\n                                         <h5 class="modal-title" id="staticBackdropLabel">Consigne</h5>\n                                         <i data-dismiss="modal" data-feather="x-octagon"></i>\n                                       </div>\n                                       <div class="modal-body text-center">\n          <img src="/media/controls/teacher.png" class="col-3" alt="Icons made by https://www.flaticon.com/authors/good-ware">' + modalContent + '</div>' + modalFooter + '</div></div></div>' + script);
        }
    };


    return {
        addEleve,
        afficheEleves,
        chooseEleve,
        connectClassToProfileEngine,
        connectStudent,
        createModal,
        openProfileEngine,
        openQrCodeModal,
        openSharingModal,
        sendEmail,
        setClasse,
        setEleve,
        showClasses,
        showClassesAdaptiveLearning,
        showClassQrCode,
        toggleAddEleve,
    };

})();

