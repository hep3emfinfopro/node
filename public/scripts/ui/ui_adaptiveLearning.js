/**
 * @classdesc fonctionnalités pour la gestion de l'adaptive learning
 * @author Aous Karoui
 * @version 1.0
 */

var ui_adaptiveLearning = (function () { // eslint-disable-line no-unused-vars

        const initParcours = function (parcoursId, adaptiveLearningIds) {
            if (adaptiveLearningIds.includes(parcoursId))
                requestRecommendations(291);
            return;
        };

        async function loadRecommandation(exoId) {
            try {
                const rawExo = await $.ajax({
                    url: '/getExerciceById',
                    method: 'POST',
                    data: {forcedExerciceId: exoId},
                    timeout: 2000
                });
                const parsedExo = JSON.parse(rawExo);
                const gameNameObject = JSON.parse(parsedExo.gameName);
                const gameNameFr = gameNameObject.fr; // La valeur en français
                const $exoLink = $('<a>', {
                    class: 'd-flex justify-content-between classe list-group-item list-group-item-action',
                    'data-bs-toggle': 'list',
                    role: 'tab',
                    href: _buildExerciseUrl(parsedExo, gameNameFr)
                }).append(
                    $('<strong>').text(gameNameFr),
                    $('<span>').text(parsedExo.description),
                    $('<h2>').html('▶️')
                );
                const $stepsContainer = $('#stepsContainer');
                $stepsContainer.append($exoLink);
                if (!$('#consigneRecommandations').length) {
                    $stepsContainer.prepend(
                        $('<h3>', {
                            id: 'consigneRecommandations',
                            text: 'Choisis un niveau parmi les jeux suivants',
                            class: 'text-center mb-5'
                        })
                    );
                }
                return true;
            } catch (error) {
                console.error('Erreur lors du chargement de la recommandation :', error);
                throw error;
            }
        }

        // Fonction utilitaire séparée
        function _buildExerciseUrl(exo, gameNameFr) {
            const templateMapping = {
                'Les Grams': 'externalParcours',
                'default': 'phaserParcours'
            };
            const template = Object.entries(templateMapping)
                    .find(([key]) => gameNameFr.includes(key))?.[1]
                || templateMapping.default;
            return `/run/${template}/${exo.id}/${exo.gameId}/evaluate`;
        };


        /**
         * Demande des recommandations au service COMPER
         * @param {number} fwId l'id du référentiel de compétences utilisé dans COMPER
         */
        const requestRecommendations = function (fwId) {
            ui_global.showLoader('Recherche de niveaux personnalisés');
            const timeoutId = setTimeout(() => {
                ui_global.showLongRequestMessage();
            }, 20000);

            wrk.send('/requestRecommendations', false, {fwId}, async function (data, success) {
                clearTimeout(timeoutId); // Annule le message d'attente prolongée si la requête réussit
                ui_global.hideLoader();

                try {
                    if (!success) throw new Error('Erreur lors de la génération des recommandations.');
                    const recomIdsArray = Array.isArray(data) ? data : Object.values(data || {});
                    await Promise.all(recomIdsArray.map((exoId) =>
                        loadRecommandation(exoId).catch((err) => {
                            console.error('Échec du chargement pour', exoId, err);
                        })
                    ));
                } catch (error) {
                    console.error(error.message || 'Erreur globale', error);
                }
            }, false);
        };

        /**
         * Vérifie l'adaptive Learning (description à compléter)
         * @param {}
         */
        const checkAdaptiveLearning = function (evaluation) {
            wrk.send('/checkAdaptiveLearning', false, {evaluation: evaluation}, function (data, success) {
                if (success) {
                    console.log('success adaptive learning');
                }
            }, false);
        };


        return {
            checkAdaptiveLearning,
            requestRecommendations,
            initParcours
        }
    }
)
();