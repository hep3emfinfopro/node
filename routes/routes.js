/**

 @classdesc Routes pour / et appel vers les autres routes

 @author Kolly Florian

 @version 1.0

 **/

const api = require('../wrk/wrkREST');

/** Préparation du WrkModule */
const WrkModule = require('../wrk/wrkModule');
const wrkModule = new WrkModule();
wrkModule.register('pixi', 'modules/pixi/templateNormal');
wrkModule.register('pixiParcours', 'modules/pixi/templateParcours');
wrkModule.register('external', 'modules/external/templateNormal');
wrkModule.register('externalParcours', 'modules/external/templateParcours');
wrkModule.register('phaser', 'modules/phaser/templateNormal');
wrkModule.register('phaserParcours', 'modules/phaser/templateParcours');

const express = require('express');

global.axios = require('axios');

global.querystring = require('querystring');

global.validator = require('validator');


// Variables globales utilisées sur le serveur
// En mode "sans parcours"
global.modeNormalPixi = util.TEMPLATE_PIXI;
global.modeNormalExternal = util.TEMPLATE_EXTERNAL;
global.modeNormalPhaser = util.TEMPLATE_PHASER;
// En mode "dans un parcours"
global.modeParcoursPixi = util.TEMPLATE_PIXI_PARCOURS;
global.modeParcoursExternal = util.TEMPLATE_EXTERNAL_PARCOURS;
global.modeParcoursPhaser = util.TEMPLATE_PHASER_PARCOURS;

// Les 4 modes de jeu possibles
global.gameModeExplore = 'explore';
global.gameModes = {
    Train: 'train',
    Evaluate: 'evaluate',
    Explore: 'explore',
    Create: 'create',
};

const router = express.Router();


router.get('/', (req, res) => res.render('index'));

router.get('/' + util.STUDENT_CONNEXION_EJS, (req, res) => res.render(util.STUDENT_CONNEXION_EJS, {
    currentPage: util.STUDENT_CONNEXION_EJS
}));
router.get('/fr', (req, res) => {

    res.cookie('lang', 'fr');

    res.redirect(req.get('referer'));

});


router.get('/de', (req, res) => {

    res.cookie('lang', 'de');

    res.redirect(req.get('referer'));

});


router.get('/it', (req, res) => {

    res.cookie('lang', 'it');

    res.redirect(req.get('referer'));

});


router.get('/en', (req, res) => {

    res.cookie('lang', 'en');

    res.redirect(req.get('referer'));

});


router.get('/logout', (req, res) => {

    req.session.destroy();

    return res.redirect('/');

});


require('./portal')(router, api);

require('./parcours')(router, api);

require('./profil')(router, api);

require('./games')(router, api, wrkModule);

require('./admin')(router, api);


module.exports = router;

