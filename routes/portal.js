/**
 @classdesc Routes pour le portail
 @author Kolly Florian, Vincent Audergon, Aous Karoui
 @version 2.0
 **/

/**
 La session peut contenir les éléments suivants:
 - token: token JWT de connexion
 - teacherID: integer pour stocker l'ID du professeur connecté
 - teacherEmail: email du professeur
 - passwordRecoveryEmail: l'email du professeur ayant demandé un nouveau mot de passe
 - classeID: integer pour stocker l'ID de la classe sélectionnée
 - eleveID: integer pour stocker l'ID de l'élève sélectionné
 - gameID: integer pour stocker l'ID du jeu sélectionné
 */

/** Constante pour jsonwebtoken */
const jwt = require('jsonwebtoken');

/** Constante pour le fichier de configuration */
const config = require('./../config.json');

/** Constante pour le fichier de traduction (mails)*/
const translations = require('./../translations.json');

/** Constante pour le model Response */
const Response = require('../models/Response.js');

/** Constante pour l'arbre JSON du PER' */
const JSON_PER = require('../models/json_per.json');
/** Constante pour le modèle de traces à envoyer au LRS */
const DATA_TO_LRS_RAW = require('./../models/dataToLRS.json');
/** Constante pour la configuration de la création d'un groupe d'élèves sur COMPER */
const COMPER_CREATE_GROUP_CONFIG = require('./../models/COMPER_CREATE_GROUP_CONFIG.json');
/** Constante pour le contenu de création d'un groupe d'élèves sur COMPER */
const COMPER_CREATE_GROUP_STATEMENTS = require('./../models/COMPER_CREATE_GROUP_STATEMENTS.json');

/** Constante pour contrôler les normes de sécurité pour les mots de passe */
const regexPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');

// API PER
const https = require('http');
const LICENCE_INFOS = require("../models/licenceInfos.json");


module.exports = (router, api) => {

    //=====================================================================
    //                               GET
    //=====================================================================

    /**
     * Rendu de la page à propos
     */
    router.get('/about', (req, res) => {
        api.requestGETfromREST('info').then(response => res.render('about', {
            rest: response.data.data.serverVersion,
            node: config.about.node,
            framework: config.about.framework,
            contact: config.about.contact
        })).catch(() => res.render('about', {
            node: config.about.node, framework: config.about.framework, contact: config.about.contact
        }));
    });

    /**
     * Le thème par défaut est celui de la HEP. Le '/peapl' permet de basculer sur le thème PEAPL dès le début
     */
    router.get('/peapl', (req, res) => {
        res.render('index', {type: 'success', info: 'shareClasse'});
    });


    /**
     * Rendu de la page d'inscription
     */
    router.get('/signup', (req, res) => res.render('signup'));


    /**
     * Rendu de la page "mot de passe oublié"
     */
    router.get('/lostpassword', (req, res) => res.render('lostPassword'));


    /**
     * Rendu de la page de modification de mot de passe (le paramètre passwordRecoveryEmail doit être défini sinon redirection vers /lostpassword).
     */
    router.get('/modifyPassword', (req, res) => {
        if (req.session.passwordRecoveryEmail) {
            res.render('modifyPassword');
        } else {
            res.redirect('/lostpassword');
        }
    });


    /**
     * Rendu de la page des classes
     */
    router.get('/classes', util.verifySession, async (req, res) => {
            try {
                delete req.session.parcoursMetaData;

                let classes = api.requestPOSTfromRESTWithID('getClasses', req.session.teacherID);
                classes = (await classes).data;
                req.session.classes = classes.data;

                let parcoursPrives = api.requestPOSTfromRESTWithID('getParcours', req.session.teacherID);
                parcoursPrives = (await parcoursPrives).data.data;
                req.session.parcoursPrives = parcoursPrives;
                let parcoursPublics = api.requestGETfromREST('getAllPublicParcours');
                parcoursPublics = (await parcoursPublics).data;

                if (classes.status === 'SUCCESS') {
                    return res.render('classes', {
                        translations: translations,
                        classes: classes.data,
                        teacherID: req.session.teacherID,
                        professeur: req.session.teacherEmail,
                        defaultStudentId: req.session.defaultStudentId,
                        roleId: req.session.teacherRole,
                        parcoursPrives: parcoursPrives,
                        parcoursPublics: parcoursPublics.data,
                        teacherPassword: req.session.teacherPassword,
                        json_per: JSON_PER,
                        URL: config.url,
                        token: req.session.token
                    });
                } else {
                    return res.render('404', {error: 'impossible d\'obtenir les classes et les élèves'});
                }
            } catch
                (e) {
                console.error(e, e.message);
                return res.render('404', {error: e.message});
            }
        }
    );

    /**
     * Rendu de la page pour ajouter une classe
     */
    router.get('/ajouterClasse', util.verifySession, (req, res) => {
        api.requestGETfromREST('getDegres').then(response => res.render('ajouterClasse', {degres: response.data.data}))
            .catch(() => res.redirect('404', {error: 'impossible d\'obtenir les degrés'}));
    });

    /**
     * Rendu de la page des jeux
     */
    router.get('/jeux', util.verifySession, async (req, res) => {

        req.session.template = global.modeNormal;
        req.session.gameMode = global.gameModes.Explore;

        // Sur la page des jeux (sans parcours), pas d'exercice forcé
        req.session.forcedExerciceId = null;

        if (typeof req.session.eleveID === 'undefined' || typeof req.session.classeID === 'undefined' || req.session.classeID === util.TEACHER_CLASS_ID) {
            if (req.session.parcoursMetaData) return res.redirect('/designParcours/' + req.session.parcours.id);
            return res.redirect('/classes');
        }
        try {
            const {disciplines, degres, langues, jeux, tags} = await multipleGetRequest({
                disciplines: 'getDisciplines',
                degres: 'getDegres',
                langues: 'getLangues',
                jeux: 'getJeux',
                tags: 'getTags'
            });
            const eleve = (await api.requestPOSTfromRESTWithID('getEleve', req.session.eleveID)).data;
            const classe = (await api.requestPOSTfromRESTWithID('getClasse', req.session.classeID)).data;

            if (eleve.status === 'SUCCESS' && classe.status === 'SUCCESS' && disciplines.status === 'SUCCESS' && degres.status === 'SUCCESS' && langues.status === 'SUCCESS' && jeux.status === 'SUCCESS' && tags.status === 'SUCCESS') {

                const currentLang = util.getLang(res);
                translateGamesDisciplines(disciplines.data, 'discipline', currentLang);
                const tabTags = translateTags(tags.data, currentLang);
                for (const jeu of jeux.data)
                    translateGameData(jeu, currentLang);

                return res.render(util.GAMES_EJS, {
                    disciplines: disciplines.data,
                    degres: degres.data,
                    langues: langues.data,
                    tags: tabTags,
                    eleve: eleve.data,
                    classe: classe.data,
                    jeux: jeux.data,
                    currentPage: util.GAMES_EJS
                });
            } else {
                return res.render('404', {error: 'impossible de charger la page des jeux'});
            }
        } catch (e) {
            console.error(e, e.message);
            return res.render('404', {error: e.message});
        }
    });

    /**
     *   Page de scénarisation d'un parcours
     */
    router.get('/designParcours/:id', util.verifySession, async (req, res) => {
        try {
            // Pour pouvoir tester les niveaux avec le compte Student du professeur
            req.session.eleveID = req.session.defaultStudentId;
            req.session.classeID = util.TEACHER_CLASS_ID;
            req.session.parcoursID = req.params.id;
            const parcoursData = (await api.getSingleParcoursById(req.session.parcoursID)).data.data;

            // Pour le templateParcours (appelé pour faire des tests de niveaux)
            setParcoursInSession(req, parcoursData);

            const {disciplines, degres, langues, jeux, tags, exos} = await multipleGetRequest({
                disciplines: 'getDisciplines',
                degres: 'getDegres',
                langues: 'getLangues',
                jeux: 'getJeux',
                tags: 'getTags',
                exos: 'getAllExercices'
            });

            if (degres.status === 'SUCCESS' && langues.status === 'SUCCESS' && jeux.status === 'SUCCESS' && exos.status === 'SUCCESS' && disciplines.status === 'SUCCESS' && tags.status === 'SUCCESS') {

                const currentLang = util.getLang(res);
                translateGamesDisciplines(disciplines.data, 'discipline', currentLang);
                const tabTags = translateTags(tags.data, currentLang);
                for (const jeu of jeux.data)
                    translateGameData(jeu, currentLang);

                return res.render('parcoursDesign', {
                    degres: degres.data,
                    json_per: JSON_PER,
                    licenceInfos: LICENCE_INFOS,
                    disciplines: disciplines.data,
                    tags: tabTags,
                    jeux: jeux.data,
                    exos: exos.data,
                    langues: langues.data,
                    parcoursData: parcoursData,
                    classes: req.session.classes,
                    defaultStudentId: req.session.defaultStudentId,
                    currentPage: util.GAMES_EJS
                });
            } else {
                return res.render('404', {error: 'impossible de charger la page des jeux'});
            }
        } catch (e) {
            console.error(e, e.message);
            return res.render('404', {error: e.message});
        }
    });

    router.get('/confirm/:code', (req, res) => {
        api.requestPOSTfromRESTWithID('confirm', req.params.code).then(response => {
            if (response.data.data !== 1) {
                console.error(response);
            }
            return res.redirect('/');
        }).catch(err => {
            console.error(err);
            return res.redirect('/');
        });
    });


    router.get('/share/:uuid', util.verifySession, (req, res) => {
        if (req.session.teacherID) {
            api.shareClasse(req.session.teacherID, req.params.uuid).then(response => {
                if (response.data.status === 'SUCCESS') {
                    return res.redirect('/classes');
                } else {
                    return res.render('404', {error: 'Could not obtain class'});
                }
            });
        } else {
            req.session.shareClass = req.params.uuid;
            req.session.save(() => {
                return res.render('index', {type: 'success', info: 'shareClasse'});
            });
        }
    });

    /**
     * Traitement du formulaire de connexion
     */
    router.get('/qrCodeAccess/:id/:teacherID', async (req, res) => {
        try {
            const classId = req.params.id;
            const teacherId = req.params.teacherID;
            req.session.teacherID = req.params.teacherID;
            const teacher = await api.requestPOSTfromRESTWithID('getTeacherById', teacherId);
            let classe = await api.requestPOSTfromRESTWithID('getClasse', classId);
            const teacherEmail = teacher.data.data.email;
            if (teacher.data.status === 'SUCCESS' && classe.data.status === 'SUCCESS') {
                jwt.sign({email: teacherEmail}, config.jwt.secret, {
                    algorithm: config.jwt.algorithm, expiresIn: config.jwt.exp, issuer: config.jwt.issuer
                }, (err, token) => {
                    if (err) {
                        console.error(err);
                        return new Response('error', 500, 'getTeacherById error', true).send(res);
                    } else {
                        // enregistrement des éléments dans la session
                        req.session.token = token;
                        classe = classe.data;
                        req.session.teacherEmail = teacherEmail;
                        res.render('qrCodeAccess', {
                            classes: classe.data,
                            currentPage: util.QR_CODE_ACCESS
                        });
                    }
                });
            }
        } catch (e) {
            console.error(e, e.message);
            return res.render('404', {error: e.message});
        }
    });

//======================================================================================
//                                        POST
//======================================================================================

    /**
     * Retourne la liste des élèves d'une classe
     */
    router.post('/eleves', async (req, res) => {
        const {idclasse} = req.body;
        const currentLang = util.getLang(res);
        if (idclasse) {
            let classe = api.requestPOSTfromRESTWithID('getClasse', idclasse);
            classe = (await classe).data;
            if (classe.status === 'SUCCESS') {
                const eleves = await api.requestPOSTfromRESTWithID('getEleves', idclasse);
                return new Response('success', 200, '', false, {eleves: eleves.data}).send(res);
            } else {
                return new Response('warning', 404, translations[currentLang].class_notfound).send(res);
            }
        } else {
            return new Response('warning', 400, translations[currentLang].invalid_class).send(res);
        }
    });

    /**
     * Traitement du formulaire d'enregistrement
     */
    router.post('/signup', (req, res) => {
        const {email, password, confirm} = req.body;
        const currentLang = util.getLang(res);
        if (password !== confirm) {
            return new Response('warning', 400, translations[currentLang].signup_errorpassword, true).send(res);
        } else {
            if (regexPassword.test(password)) {
                const code = util.generateRandomCode(16);
                api.doSignup(email, password, code).then(response => {
                    if (response.data.status === 'SUCCESS') {
                        const currentLang = util.getLang(res);
                        const subject = translations[currentLang].signup_subject;
                        const text = translations[currentLang].signup_text;
                        util.sendEmail(email, subject, `${text}: ${config.url}/confirm/${code}`, `<p>${text}: <a href="${config.url}/confirm/${code}">${config.url}/confirm</a></p>`, currentLang);
                        return new Response('success', 200, translations[currentLang].signup_success, true).send(res);
                    } else if (response.data.message.includes('Duplicate entry')) {
                        return new Response('warning', 400, translations[currentLang].signup_erroruser, true).send(res);
                    } else if (response.data.errorCode === 405) {
                        return new Response('warning', 400, translations[currentLang].signup_errormail, true).send(res);
                    } else {
                        return new Response('warning', 500, translations[currentLang].signup_error, true).send(res);
                    }
                }).catch((e) => {
                    console.error(e.message);
                    return new Response('warning', 500, translations[currentLang].signup_error, true).send(res);
                });
            } else {
                return new Response('warning', 400, translations[currentLang].signup_errorsecurity, true).send(res);
            }
        }
    });

    /**
     * Traitement du formulaire de connexion
     */
    router.post('/login', (req, res) => {
        const currentLang = util.getLang(res);
        // récupération des données du formulaire
        const {email, password} = req.body;
        // requête de connexion
        api.verifyLogin(email, password).then(response => {
            if (response.data.status === 'SUCCESS') {
                if (!response.data.data.isConfirmed)
                    return new Response('warning', 400, translations[currentLang].login_notconfirmed, true).send(res);
                // enregistrement du token JWT
                jwt.sign({email: email}, config.jwt.secret, {
                    algorithm: config.jwt.algorithm, expiresIn: config.jwt.exp, issuer: config.jwt.issuer
                }, (err, token) => {
                    if (err) {
                        console.error(err);
                        return new Response('error', 500, translations[currentLang].login_error, true).send(res);
                    } else {
                        // enregistrement des éléments dans la session
                        req.session.token = token;
                        req.session.teacherID = response.data.data.id;
                        req.session.teacherEmail = response.data.data.email;
                        req.session.teacherRole = response.data.data.role;
                        req.session.eleveID = response.data.data.defaultStudentId;
                        // On garde defaultStudentId dans la session pour le ré-utiliser dans la page parcoursDesign (pour tester des exos)
                        req.session.defaultStudentId = response.data.data.defaultStudentId;
                        // si la session contient une classe partagée (via le lien donné par shareClasse)
                        if (req.session.shareClass) {
                            // enregistrement de la classe
                            api.shareClasse(response.data.data.id, req.session.shareClass).then(response => {
                                req.session.shareClass = undefined;
                                // sauvegarde de la session
                                req.session.save(() => {
                                    if (response.data.status === 'SUCCESS') {
                                        return new Response('success', 200, translations[currentLang].login_success).send(res);
                                    } else if (response.data.errorCode === 406) {
                                        return new Response('error', 200, translations[currentLang].share_expired, true).send(res);
                                    } else {
                                        return new Response('error', 200, translations[currentLang].share_classnotfound, true).send(res);
                                    }
                                });
                            }).catch(e => {
                                console.error(e.message);
                                return new Response('error', 500, translations[currentLang].share_error, true).send(res);
                            });
                        } else {
                            req.session.save(() => {
                                return new Response('success', 200, translations[currentLang].login_success).send(res);
                            });
                        }
                    }
                });
                // en cas d'erreur, renvoyer l'index
            } else if (response.data.status === 'FAIL') {
                return new Response('error', 400, translations[currentLang].login_wrong, true).send(res);
            } else {
                return new Response('error', 500, translations[currentLang].login_error, true).send(res);
            }
        }).catch((e) => {
            console.error(e.message);
            return new Response('error', 500, translations[currentLang].login_error, true).send(res);
        });
    });

    /**
     * Ajout d'une classe
     */
    router.post('/addClasse', util.verifySession, (req, res) => {
        let {className, degre, eleves} = req.body;
        const currentLang = util.getLang(res);
        if (degre) {
            if (className.trim() !== '') {
                if (eleves === undefined) eleves = [];
                api.addClasse(className.trim(), degre, req.session.teacherID, eleves).then(response => {
                    if (response.data.status === 'SUCCESS') {
                        return new Response('success', 200, translations[currentLang].class_created, true).send(res);
                    } else {
                        return new Response('error', 500, translations[currentLang].class_errorcreate).send(res);
                    }
                }).catch((e) => {
                    console.error(e.message);
                    return new Response('error', 500, e.message).send(res);
                });
            } else {
                return new Response('warning', 400, translations[currentLang].class_invalidname).send(res);
            }
        } else {
            return new Response('warning', 400, translations[currentLang].class_missingdegre).send(res);
        }
    });

    /**
     * Ajout d'un parcours (création du scénario) : à corriger car incomplet
     */
    router.post('/scenarioParcours', util.verifySession, (req, res) => {
        let {degre} = req.body;
        const currentLang = util.getLang(res);
        if (degre) {
            api.addClasse(className.trim(), degre, req.session.teacherID, eleves).then(response => {
                if (response.data.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].class_created, true).send(res);
                } else {
                    return new Response('error', 500, translations[currentLang].class_errorcreate).send(res);
                }
            }).catch((e) => {
                console.error(e.message);
                return new Response('error', 500, e.message).send(res);
            });
        } else {
            return new Response('warning', 400, translations[currentLang].class_missingdegre).send(res);
        }
    });

    /**
     * Ajout d'un élève
     */
    router.post('/addEleve', util.verifySession, async (req, res) => {
        const {eleveName, classe} = req.body;
        const currentLang = util.getLang(res);
        if (eleveName && eleveName.trim() !== '') {
            const eleves = (await api.requestPOSTfromRESTWithID('getEleves', classe)).data.data;
            let ok = true;
            for (let eleve of eleves) {
                if (eleve.nom.trim().toLowerCase() === eleveName.trim().toLowerCase()) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                api.addEleve(eleveName.trim(), classe).then(() => {
                    return new Response('success', 200, translations[currentLang].student_created).send(res);
                }).catch((e) => {
                    console.error(e.message);
                    return new Response('error', 500, e.message).send(res);
                });
            } else {
                return new Response('error', 400, translations[currentLang].student_already_exists, true).send(res);
            }
        } else {
            return new Response('warning', 400, translations[currentLang].invalid_name).send(res);
        }
    });


    /**
     * Envoie un email de vérification
     */
    router.post('/sendVerificationmail', (req, res) => {
        const {email} = req.body;
        const currentLang = util.getLang(res);
        if (email !== undefined && validator.isEmail(email)) {
            const code = util.generateRandomCode(8);
            api.addCodeToProfesseur(email, code).then(async response => {
                if (response.data.status === 'SUCCESS') {
                    const subject = translations[currentLang].verificationemail_subject;
                    const text = translations[currentLang].verificationemail_text;
                    // const ok = await util.sendEmail(email, subject, `${text}: ${code}`, `<p>${text}: ${code}</p>`, currentLang);
                    util.sendEmail(email, subject, `${text}: ${code}`, `<p>${text}: ${code}</p>`, currentLang);
                    return new Response('success', 200, translations[currentLang].mail_success, true).send(res);
                    // return new Response(ok ? 'success' : 'error', ok ? 200 : 500, ok ? translations[currentLang].mail_success : translations[currentLang].mail_error, false, { ok: ok }).send(res);
                } else {
                    return new Response('warning', 404, translations[currentLang].account_notfound).send(res);
                }
            }).catch(e => {
                console.error(e);
                return new Response('error', 500, e.message).send(res);
            });
        } else {
            return new Response('warning', 400, translations[currentLang].mail_invalid).send(res);
        }
    });

    /**
     * Envoie un email de partage de classe
     */
    router.post('/sendSharingMail', util.verifySession, async (req, res) => {
        const {email, text, link} = req.body;
        let response;
        const currentLang = util.getLang(res);
        if (email !== undefined && link !== undefined && validator.isEmail(email)) {
            const subject = translations[currentLang].share_subject;
            const sharetext = translations[currentLang].share_text_1;
            const sharetext_end = translations[currentLang].share_text_2;
            const ok = await util.sendEmail(email, subject, `${text} \n\n${sharetext}: ${link}. ${sharetext_end}: ${req.session.teacherEmail}.`, `${text} <br/><div>${sharetext}: <a href=${link}>${link}</a>. ${sharetext_end}: ${req.session.teacherEmail}.</div>`, currentLang);
            response = new Response(ok ? 'success' : 'error', ok ? 200 : 500, ok ? translations[currentLang].mail_success : translations[currentLang].mail_error);
        } else {
            response = new Response('warning', 400, translations[currentLang].mail_invalid);
        }
        return response.send(res);
    });


    /**

     * Vérifie le code de modification de mot de passe. Si le code est correct, indique l'email dans la session (session.passwordReciveryEmail)

     */

    router.post('/lostpassword', (req, res) => {

        const {email, code} = req.body;

        const currentLang = util.getLang(res);

        if (code && email && validator.isEmail(email)) {

            api.verifyCodeForRecovery(code, email).then(response => {

                if (response.data.status === 'SUCCESS' && response.data.data === 'OK') {

                    req.session.passwordRecoveryEmail = email;

                    return new Response('info', 200, 'info').send(res);

                } else {

                    return new Response('error', 400, translations[currentLang].lostpassword_invalidcode).send(res);

                }

            });

        } else {

            return new Response('warning', 400, translations[currentLang].mail_invalid).send(res);

        }

    });


    /**
     * Modifie le mot de passe du compte lié à l'adresse stockée dans la session (session.passwordReciveryEmail)
     */
    router.post('/modifypassword', (req, res) => {
        const {password, confirm} = req.body;
        const currentLang = util.getLang(res);
        if (req.session.passwordRecoveryEmail) {
            if (password === confirm) {
                if (regexPassword.test(password)) {
                    api.modifyPassword(req.session.passwordRecoveryEmail, password).then(response => {
                        if (response.data.status === 'SUCCESS' && response.data.data === 'OK') {
                            delete req.session.passwordRecoveryEmail;
                            return new Response('success', 200, translations[currentLang].lostpassword_passwordmodified, true).send(res);
                        } else {
                            return new Response('error', 500, translations[currentLang].intern_error).send(res);
                        }
                    }).catch(e => {
                        console.error(e);
                        return new Response('error', 500, translations[currentLang].intern_error).send(res);
                    });
                } else {
                    return new Response('warning', 400, translations[currentLang].signup_errorsecurity, true).send(res);
                }
            } else {
                return new Response('warning', 400, translations[currentLang].lostpassword_wrongpassword).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired).send(res);
        }
    });

    /**
     * Définit l'élève et la classe dans la session et vérifie que l'utilisateur y ait bien accès
     */
    router.post('/setEleve', util.verifySession, async (req, res) => {
        const {eleveid, classeid} = req.body;

        const currentLang = util.getLang(res);
        let ok = false;
        const data = (await api.requestPOSTfromRESTWithID('getClasses', req.session.teacherID)).data;
        const dataEleve = (await api.requestPOSTfromRESTWithID('getEleve', eleveid)).data;
        if (data.status === 'SUCCESS') {
            if (dataEleve.status === 'SUCCESS') {
                for (let classe of data.data) {
                    if (classe.id === parseInt(classeid) && dataEleve.data.fk_classe === parseInt(classeid)) {
                        ok = true;
                        break;
                    }
                }
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(true);
        }
        if (ok) {
            req.session.eleveID = parseInt(eleveid);
            req.session.classeID = parseInt(classeid);
            req.session.save((err) => {
                if (err) {
                    return new Response('error', 500, '').send(res);
                } else {
                    return new Response('success', 200, '').send(res);
                }
            });
        } else {
            return new Response('error', 403, translations[currentLang].forbidden, true).send(res);
        }
    });

    /**
     * Ajoute une activité débranchée ou externe dans la base de données
     */
    router.post('/createUnpluggedActivity', util.verifySession, async (req, res) => {
        const {exercice, gameId, exoName} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.createUnpluggedActivity(exercice, gameId, req.session.eleveID, exoName)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].exercice_add_success, false, {id: data.data}).send(res);
        } else {
            new Response('error', 500, data, true).send(res);
        }
    });

    /**
     * Supprime un exercice depuis la base de données
     */
    router.post('/deleteExercice', util.verifySession, async (req, res) => {
        let canDelete = true;
        let parcoursPrives = api.requestPOSTfromRESTWithID('getParcours', req.session.teacherID);
        parcoursPrives = (await parcoursPrives).data;
        const parcoursIncludingExo = [];
        for (const parcours of parcoursPrives.data) {
            try {
                for (const step of JSON.parse(parcours.steps)) {
                    if (step.id == req.body.exoId) {
                        canDelete = false;
                        parcoursIncludingExo.push(parcours.nom);
                        break;
                    }
                }
            } catch (e) {
                console.log(e);
            }
        }
        const currentLang = util.getLang(res);
        if (canDelete) {
            api.deleteExercice(req.body.exoId).then(response => {
                if (response.data.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].delete_exercice_success).send(res);
                } else {
                    return new Response('error', 404, translations[currentLang].delete_exercice_error).send(res);
                }
            }).catch((e) => {
                return new Response('error', 200, translations[currentLang].delete_exercice_error).send(res);
            });
        } else {
            let errorText = translations[currentLang].unplugged_already_exists;
            errorText += '\n' + JSON.stringify(parcoursIncludingExo);
            return new Response('error', 400, errorText, true).send(res);
        }
    });

    /**
     * Code temporaire pour tester l'envoi des traces
     */
    router.get('/sendParcoursBetaData', async (req, res) => {
        function sendTracks(key, finalTracks) {
            if (finalTracks.hasOwnProperty(key)) {
                const value = finalTracks[key];
                // Pour des raisons de sécurité, on copie l'objet d'origine
                const dataToLRS = structuredClone(DATA_TO_LRS_RAW);

                const inputDate = value.date;
                const inputTime = value.startTime;
                const trackTimestamp = new Date(`${inputDate}T${inputTime}`);
                const outputDate = trackTimestamp.toISOString();
                const output = outputDate.slice(0, 19) + outputDate.slice(23);
                // const formattedTimestamp = new Date(trackTimestamp).toISOString().replace(/\.000Z$/, '+0200');

                dataToLRS.actor.account.name = 'student_id:' + value.studentId;
                dataToLRS.object.id = 'https://hep3.emf-infopro.ch:' + value.exoId;
                dataToLRS.object.definition.description['fr-FR'] = value.description;
                dataToLRS.object.definition.description.parcours = value.parcoursName;
                dataToLRS.object.definition.extensions['https://comper.fr/xapi/nodeId'] = 'GamesHub:' + value.exoId;

                const evaluation = Number(value.evaluation);
                // Conversion nécessaire pour s'adapter au format LRS
                if (evaluation > 0) {
                    if (evaluation === 2) {
                        dataToLRS.result.score.scaled = 1;
                        dataToLRS.result.success = true;
                    } else if (evaluation === 1) dataToLRS.result.score.scaled = 0.5;
                }

                var config = {
                    method: 'post',
                    url: 'https://compervm-dev.liris.cnrs.fr/trax/api/gateway/clients/default/stores/default/xapi/statements',
                    headers: {
                        'Content-Type': 'application/json',
                        'Response-Type': 'application/json',
                        'X-Experience-API-Version': '1.0.0',
                        'Authorization': 'Basic dHJheC1jb21wZXI6IW1kcDIwMjBUUkFYLUNPTVBFUi1MUlM='
                    },
                    auth: {
                        username: 'trax-comper', password: '!mdp2020TRAX-COMPER-LRS'
                    },
                    data: JSON.stringify(dataToLRS)
                };
                axios(config)
                    .then(function (response) {
                        console.log(JSON.stringify(response.data));
                        res.write(JSON.stringify(response.data));
                        res.end();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }

        try {
            let tracks = api.requestGETfromREST('getParcoursBetaTracks');
            tracks = (await tracks).data;
            if (tracks.status === 'SUCCESS') {
                const parsedTracks = JSON.parse(tracks.data);
                const finalTracks = parsedTracks.tracks;
                for (const key in finalTracks) {
                    setTimeout(() => {
                        sendTracks(key, finalTracks);
                    }, 1500);
                }
                return new Response('success', 200, 'ok').send(res);
            } else {
                return new Response('error', 404, 'erreur').send(res);
            }
        } catch (e) {
            console.error(e, e.message);
            return res.render('404', {error: e.message});
        }
    });

    /**
     * Défini la classe dans la session et vérifie que l'utilisateur y ait bien accès
     */
    router.post('/setClasse', util.verifySession, async (req, res) => {

        const {classeid} = req.body;
        const currentLang = util.getLang(res);
        let ok = false;
        const data = (await api.requestPOSTfromRESTWithID('getClasses', req.session.teacherID)).data;
        if (data.status === 'SUCCESS') {
            for (let classe of data.data) {
                if (classe.id === parseInt(classeid)) {
                    ok = true;
                    break;
                }
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(true);
        }
        if (ok) {
            req.session.classeID = parseInt(classeid);
            req.session.save((err) => {
                if (err) {
                    return new Response('error', 500, '').send(res);
                } else {
                    return new Response('success', 200, '').send(res);
                }
            });
        } else {
            return new Response('error', 403, translations[currentLang].forbidden, true).send(res);
        }
    });

    router.post('/checkPasswordForId', util.verifySession, async (req, res) => {
        const {teacherPwd} = req.body;
        const checked = await api.checkPasswordForId(teacherPwd, req.session.teacherID);
        console.log(checked);
        if (checked)
            return res.redirect('/classes');

    });

    /***********************************************************
     -----------------Page Adaptive Learning------------------
     **********************************************************/
    router.get('/adaptiveLearning', util.verifySession, async (req, res) => {
            try {
                let classes = api.requestPOSTfromRESTWithID('getClasses', req.session.teacherID);
                classes = (await classes).data;
                req.session.classes = classes.data;
                if (req.session.teacherRole && (req.session.teacherRole === 2 || req.session.teacherRole === 3)) {
                    if (classes.status === 'SUCCESS') {
                        return res.render(util.ADAPTIVE_LEARNING_EJS, {
                            classes: classes.data,
                            teacher: req.session.teacherEmail,
                            currentPage: util.ADAPTIVE_LEARNING_EJS
                        });
                    } else {
                        return res.render('404', {error: 'impossible d\'obtenir les classes et les élèves'});
                    }
                }
            } catch (e) {
                console.log(e);
                return res.redirect('/classes');
            }
        }
    );

    /**
     * Rattache une classe de GamesHub à un référentiel sur Profile Engine. Techniquement, consiste à créer un groupe (une classe) d'étudiants sur Profile Engine (projet Comper) pour un référentiel donné.
     */
    router.post('/connectClassToProfileEngine', util.verifySession, async (req, res) => {
        try {
            const {fwkId, classId, className, studentsList} = req.body;
            // Pour des raisons d'intégrité', on copie les objets d'origine
            const groupConfig = structuredClone(COMPER_CREATE_GROUP_CONFIG);
            const groupData = structuredClone(COMPER_CREATE_GROUP_STATEMENTS);
            const parsedStudentsList = JSON.parse(studentsList);

            groupData.fwid = fwkId;
            groupData.groupName = className;
            groupData.platformGroupId += `_${classId}_${fwkId}`;

            for (var student of parsedStudentsList) {
                const studentId = student.id.replace("eleve", util.TRACKS_ID_PREFIX);
                const studentData = {
                    "username": studentId,
                    "user": studentId,
                    "forename": student.name,
                    "name": ""
                };
                groupData.students.push(studentData);
            }

            const token = await jwt.sign(groupData, config.jwtCOMPER.secret, {
                algorithm: config.jwtCOMPER.algorithm,
                expiresIn: config.jwtCOMPER.exp
            });
            groupConfig.headers.Authorization += token;
            console.log(token);

            try {
                const response = await axios.request(groupConfig);
                return res.status(200).json(response.data);
            } catch (error) {
                console.error('Erreur lors de la requête axios:', error.message);
                return res.status(500).json({
                    error: 'Erreur lors de la requête vers l’API externe',
                    details: error.response?.data || error.message
                });
            }

        } catch (e) {
            console.log(error);
        }
    });

    router.post('/openProfileEngine', util.verifySession, (req, res) => {
        const {fwkId, classId, teacherId} = req.body;
        try {
            const data = {
                fwid: "" + fwkId,
                // FIXME teacherId should be set as the username; refactor /connectClassToProfileEngine accordingly
                // username: req.session.teacherEmail.substring(0, req.session.teacherEmail.indexOf('@')),
                username: "aousKaroui",
                role: "teacher",
                platformGroupId: "gameshub:group" + "_" + classId + "_" + fwkId,
                platform: "gameshub",
                homepage: config.url,
                exp: Date.now() + 3000
                // we explicitly set the "exp" (Expiration Time) claim, because we don't let jsonwebtoken generate it
            };

            jwt.sign(data, config.jwtCOMPER.secret, {
                algorithm: config.jwtCOMPER.algorithm,
                noTimestamp: true // we don't want jsonwebtoken to generate the "iat" (Issued At) claim:
                // the "iat" claim can cause the JWT to be rejected by the COMPER server if the Node server time is out of sync
            }, (err, token) => {
                res.json({token});
            });
        } catch (e) {
            console.log(e);
        }
    });


//==================================================================
//====  Fonctions diverses utilisées par les requêtes ci-dessus ====
//==================================================================

    /**
     * Exécute la requête asynchrone "requestGETfromREST" plusieurs fois
     */
    const multipleGetRequest = async (endpoints) => {
        const requests = Object.entries(endpoints).map(async ([key, value]) => {
            const response = await api.requestGETfromREST(value);
            return [key, response.data];
        });
        const resultArray = await Promise.all(requests);
        return resultArray.reduce((obj, [key, value]) => {
            obj[key] = value;
            return obj;
        }, {});
    };

    /**
     * Traduit les disciplines du jeu
     */
    const translateGamesDisciplines = (disciplines, field, currentLang) => {
        for (const discipline of disciplines) {
            discipline[field] = JSON.parse(discipline[field])[currentLang];
        }
    };

    /**
     * Traduit les tags
     */
    const translateTags = (tags, currentLang) => {
        return tags.map(tag => JSON.parse(tag)[currentLang]);
    };

    /**
     * Traduit les données du jeu
     */
    const translateGameData = (game, currentLang) => {
        game.nom = JSON.parse(game.nom)[currentLang];
        game.description = JSON.parse(game.description)[currentLang];
        game.objectif = JSON.parse(game.objectif)[currentLang];
        game.image = 'data:image/jpg;base64,' + game.image;
        game.lstTags = translateTags(game.lstTags, currentLang);
    };

    /**
     * Ré-initialise le parcours dans la session
     */
    function setParcoursInSession(req, parcours) {
        req.session.parcours = {};
        req.session.parcours = parcours;
    }

};