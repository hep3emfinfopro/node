/**
 @classdesc Routes pour les jeux
 @author Kolly Florian
 @version 1.0
 **/

const textToSpeech = require('@google-cloud/text-to-speech');

/** Constante pour le model Response */
const Response = require('../models/Response.js');
/** Constante pour le fichier de traduction (mails)*/
const translations = require('./../translations.json');

const {body} = require('express-validator/check');

const clientTTS = new textToSpeech.TextToSpeechClient();

var currentGame;

/** Constantes de paramétrage pour l'envoi des traces au LRS */
const LRS_URL = 'https://compervm-dev.liris.cnrs.fr/trax/api/gateway/clients/default/stores/default/xapi/statements';
const LRS_HEADERS = {
    'Content-Type': 'application/json',
    'Response-Type': 'application/json',
    'X-Experience-API-Version': '1.0.0',
    'Authorization': 'Basic dHJheC1jb21wZXI6IW1kcDIwMjBUUkFYLUNPTVBFUi1MUlM='
};
const LRS_AUTH = {
    username: 'trax-comper',
    password: '!mdp2020TRAX-COMPER-LRS'
};
/** Constante pour le modèle de traces à envoyer au LRS */
const DATA_TO_LRS_RAW = require('./../models/dataToLRS.json');

module.exports = (router, api, wrkModule) => {

    router.post('/sendTrackToLRS', async (req, res) => {
        try {
            const evaluation = Number(req.body.evaluation);
            const exo = (await api.getExerciceById(req.session.forcedExerciceId)).data;
            const dataToLRS = JSON.parse(JSON.stringify(DATA_TO_LRS_RAW)); // Copie de l'objet d'origine
            const currentTimestamp = Date.now();
            const formattedTimestamp = new Date(currentTimestamp).toISOString().replace(/\.000Z$/, '+0200');

            dataToLRS.actor.account.name += req.session.eleveID;
            dataToLRS.object.id += req.session.forcedExerciceId;
            dataToLRS.object.definition.description['fr-FR'] = exo.data.description;
            dataToLRS.object.definition.description.parcours = req.session.parcours.nom;
            dataToLRS.timestamp = formattedTimestamp;
            dataToLRS.result.score.scaled = evaluation;
            dataToLRS.object.definition.extensions['https://comper.fr/xapi/nodeId'] = 'GamesHub:' + req.session.forcedExerciceId;
            // Conversion nécessaire pour s'adapter au format LRS
            if (evaluation > 0 && evaluation <= 2) {
                dataToLRS.result.score.scaled = evaluation / 2;
                dataToLRS.result.success = evaluation === 2;
            }

            const config = {
                method: 'post',
                url: LRS_URL,
                headers: LRS_HEADERS,
                auth: LRS_AUTH,
                data: JSON.stringify(dataToLRS)
            };
            axios(config)
                .then(function (response) {
                    console.log(JSON.stringify(response.data));
                    res.write(JSON.stringify(response.data));
                    res.end();
                })
                .catch(function (error) {
                    console.log(error);
                });
        } catch (error) {
            console.log(error);
            // Gérer l'erreur et renvoyer une réponse appropriée
            res.status(500).send('Une erreur est survenue');
        }
    });

    /**
     *  Fonction préalable à l'appel "/jeu/:jeu"
     *  permet de définir le mode (sans parcours / avec parcours)...
     */
    router.get(`/${util.RUN_STEP_KEYWORD}/:template/:exoId/:jeu/:gameMode`, util.verifySession, (req, res) => {
        req.session.template = req.params.template;
        req.session.forcedExerciceId = req.params.exoId;
        req.session.gameMode = req.params.gameMode;
        res.writeHead(301,
            {Location: '/jeu/' + req.params.jeu}
        );
        res.end();
    });

    router.get('/jeu/:jeu', util.verifySession, (req, res) => {
        if (req.session.eleveID) {
            let eleve;
            let idExoLegram = null;
            let DAT_load = true;
            const classId = req.session.classeID;
            if (util.WITHOUT_DAT_CLASS_IDS.includes(classId))
                DAT_load = false;
            api.getEleve(req.session.eleveID).then(response => {
                if (response.data.status === 'SUCCESS') {
                    eleve = response.data.data;
                    return api.getJeuByID(req.params.jeu);
                } else {
                    return res.redirect('/classes');
                }
            }).then(response => {
                if (response.data.status === 'SUCCESS') {
                    const jeu = response.data.data;
                    jeu.inputs = JSON.parse(jeu.json).inputs;
                    currentGame = req.params.jeu;
                    req.session.gameID = currentGame;
                    req.session.save(async (err) => {
                        if (err) {
                            return res.render('404', {error: 'La session n\'a pas pu être enregistrée'});
                        }
                        // Appel depuis RunParcours
                        else {
                            let module;
                            if (req.session.template === util.TEMPLATE_PIXI_PARCOURS)
                                module = wrkModule.getModule(util.TEMPLATE_PIXI_PARCOURS);
                            else if (req.session.template === util.TEMPLATE_PHASER_PARCOURS)
                                module = wrkModule.getModule(util.TEMPLATE_PHASER_PARCOURS);
                            else if (req.session.template === util.TEMPLATE_EXTERNAL_PARCOURS) {
                                module = wrkModule.getModule(util.TEMPLATE_EXTERNAL_PARCOURS);
                                idExoLegram = (await api.getExerciceById(req.session.forcedExerciceId)).data.data.exercice;
                            }
                            // Appel en mode jeu libre
                            else {
                                module = wrkModule.getModule(jeu.module);
                                // Cette condition vérifie que l'on n'est pas en train de tester un niveau par un enseignant
                                if (classId !== undefined && classId !== util.TEACHER_CLASS_ID) {
                                    idExoLegram = 'false';
                                    req.session.forcedExerciceId = null;
                                }
                            }
                            if (module) {
                                return res.render(module.template, {
                                    jeu,
                                    eleve,
                                    gameMode: req.session.gameMode,
                                    parcours: req.session.parcours,
                                    forcedExerciceId: req.session.forcedExerciceId,
                                    DAT_load,
                                    idExoLegram,
                                    language: util.getLang(res),
                                    dataPage: util.DATA_PAGE_STUDENT,
                                    currentPage: util.FREE_GAME_MODE
                                });
                            } else {
                                // TODO: should not be a 404 error
                                return res.render('404', {error: 'Aucun module de rendu trouvé pour jeu.'});
                            }
                        }
                    });
                } else {
                    return res.render('404', {error: 'Impossible d\'obtenir le jeu'});
                }
            });
        } else {
            return res.redirect('/');
        }
    });

    router.post('/textToSpeech', async (req, res) => {
        const {text, languageCode} = req.body;
        const request = {
            input: {text},
            voice: {
                languageCode: languageCode,
                name: "fr-FR-Standard-D"
            },
            audioConfig: {audioEncoding: 'MP3'}
        };

        try {
            const [response] = await clientTTS.synthesizeSpeech(request);
            const data = JSON.parse(JSON.stringify(response.audioContent)).data;
            res.writeHead(200, {
                'Content-Type': 'audio/mpeg'
            });
            res.write(JSON.stringify(data));
            res.end();
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getSessionGameId', util.verifySession, (req, res) => {
        return new Response('success', 200, 'ok', false, {
            gameId: req.session.gameID
        }).send(res);
    });

    router.post('/getJeu', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        if (req.session.gameID) {
            const data = (await api.getJeuByID(req.session.gameID)).data;
            if (data.status === 'SUCCESS') {
                const game = data.data;
                game.inputs = JSON.parse(game.json).inputs;
                const dataEleve = (await api.getEleve(req.session.eleveID)).data;
                if (dataEleve.status === 'SUCCESS') {
                    return new Response('success', 200, translations[currentLang].game_loaded, false, {
                        game,
                        degre: dataEleve.data.fk_degre,
                        language: util.getLang(res)
                    }).send(res);
                } else {
                    return new Response('error', 404, translations[currentLang].student_notfound, true).send(res);
                }
            } else {
                return new Response('error', 404, translations[currentLang].game_notfound, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    router.post('/getExercice', (req, res) => {
        const currentLang = util.getLang(res);
        try {
            api.getExercice(currentGame, req.body.degre).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    // return new Response('error', 404, translations[currentLang].game_notfound, true).send(res);
                    return res.render('404', {error: 'impossible de charger la page des jeux'});
                    // Swal.fire({
                    //     title: 'error',
                    //     text: 'Timeout',
                    // });
                    // res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getExerciceById', (req, res) => {
        try {
            api.getExerciceById(req.body.forcedExerciceId).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/getExercicesEleves', (req, res) => {
        try {
            api.getExerciceEleves(currentGame, req.body.degre).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            console.error(e);
            res.status(404).send();
        }
    });

    router.post('/addExercice', util.verifySession, async (req, res) => {
        let fk_jeu;
        const {exercice, degre, forcedState, gameId} = req.body;
        req.session.gameID ? fk_jeu = req.session.gameID : fk_jeu = gameId;
        const currentLang = util.getLang(res);
        if (fk_jeu) {
            const data = (await api.addExercice(exercice, fk_jeu, req.session.eleveID, degre, forcedState)).data;
            if (data.status === 'SUCCESS') {
                new Response('success', 200, translations[currentLang].exercice_add_success, false, {id: data.data}).send(res);
            } else {
                new Response('error', 500, data, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    /**
     * Ajoute une statistique par rapport au jeu et l'élève dans la session
     */
    router.post('/addStats', util.verifySession, async (req, res) => {
        const {mode, gameId, forcedExerciceId} = req.body;
        const currentLang = util.getLang(res);
        let parcoursId = null;
        if (req.session.parcours)
            parcoursId = req.session.parcours.id;
        // Si on ne passe pas par la page /run... (exp : unplugged Activity)
        // Todo passer par la page run ou mettre tout dans une fonction équivalente
        if (req.body.gameId) {
            req.session.gameID = req.body.gameId;
            req.session.forcedExerciceId = req.body.forcedExerciceId;
            req.session.template = global.modeParcoursExternal;
        }

        if (req.session.eleveID && req.session.gameID) {
            // On récupère l'exercice courant pour l'ajouter aux stats
            let exo;
            let idExo = null;
            // Si on est en mode Parcours, on récupère l'exercice forcé
            if (req.session.template === global.modeParcoursPixi || req.session.template === global.modeParcoursExternal || req.session.template === global.modeParcoursPhaser)
                exo = (await api.getExerciceById(req.session.forcedExerciceId)).data;
            // Sinon
            else
                exo = (await api.getExercice(req.session.gameID, 3)).data;

            if (exo.status === 'SUCCESS')
                idExo = exo.data.id;

            const data = (await api.addStats(mode, req.session.gameID, req.session.eleveID, idExo, parcoursId)).data;
            if (data.status === 'SUCCESS') {
                new Response('success', 200, translations[currentLang].stats_success, false, {id: data.data}).send(res);
            } else {
                new Response('error', 500, translations[currentLang].stats_error, true).send(res);
            }
        } else {
            return new Response('error', 498, translations[currentLang].session_expired, true).send(res);
        }
    });

    /**
     * Mets à jour une statistique avec l'évaluation
     */
    router.post('/updateStats', util.verifySession, async (req, res) => {
        const {id, evaluation, details} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.updateStats(id, evaluation, details)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].stats_success).send(res);
        } else {
            new Response('error', 500, translations[currentLang].stats_error, true).send(res);
        }
    });

    /**
     * Signal un jeu faux
     */
    router.post('/signaler', util.verifySession, async (req, res) => {
        const {ex, msg, password} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.signaler(msg, password, ex, req.session.teacherID)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].signal_success).send(res);
        } else if (data.status === 'WARNING') {
            new Response('warning', 200, translations[currentLang].signal_bad_password).send(res);
        } else {
            new Response('error', 500, translations[currentLang].signal_error, true).send(res);
        }
    });

    router.post('/getSprites', (req, res) => {
        try {
            api.getSprites(req.body.gameId).then(response => {
                if (response.data.status === 'SUCCESS') {
                    res.writeHead(200);
                    res.write(JSON.stringify(response.data.data));
                    res.end();
                } else {
                    res.status(404).send();
                }
            });
        } catch (e) {
            res.status(404).send();
        }
    });


    /**
     * Crée une nouvelle statistique d'utilisation de fonctionnalité additionnelle (DAT)
     */
    router.post('/newDATEntry', util.verifySession, async (req, res) => {
        const {datFunction} = req.body;
        const currentLang = util.getLang(res);
        const gameModeArray = Object.values(global.gameModes);
        // FIXME gameModeIndex à corriger car il envoie toujours 1 (mode entrainer)
        const gameModeIndex = gameModeArray.indexOf(req.session.gameMode);
        const data = (await api.newDATEntry(
            req.session.eleveID,
            req.session.forcedExerciceId,
            gameModeIndex,
            req.session.parcours.id,
            datFunction
        )).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].stats_success, false, data).send(res);
        } else {
            new Response('error', 500, translations[currentLang].stats_error, true).send(res);
        }
    });

    /**
     * Met à jour une statistique d'utilisation de fonctionnalité additionnelle (DAT)
     */
    router.post('/updateDATEntry', util.verifySession, async (req, res) => {
        const {statId, content} = req.body;
        const currentLang = util.getLang(res);
        const data = (await api.updateDATEntry(statId, content)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].stats_success).send(res);
        } else {
            new Response('error', 500, translations[currentLang].stats_error, true).send(res);
        }
    });

    /**
     * Met à jour la collection d'un élève
     */
    router.post('/updateStudentCollection', util.verifySession, async (req, res) => {
        const collectionData = req.body;
        const studentId = req.session.eleveID;
        const result = (await api.updateStudentCollection(studentId, JSON.stringify(collectionData))).data;
        if (result.status === 'SUCCESS') {
            new Response('success', 200, 'collection updated with success', false, {collection: result.data}).send(res);
        } else {
            new Response('error', 500, 'error while updating collection', true).send(res);
        }
    });

    /**
     * Récupère la collection d'un élève
     */
    router.post('/getStudentCollection', util.verifySession, async (req, res) => {
        const studentId = req.session.eleveID;
        const result = (await api.getStudentCollection(studentId)).data;
        if (result.status === 'SUCCESS') {
            new Response('success', 200, 'student collection', false, {collection: result.data}).send(res);
        } else {
            new Response('error', 500, 'error while getting collection', true).send(res);
        }
    });
};
