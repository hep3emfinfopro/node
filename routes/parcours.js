/**
 @classdesc Routes pour les parcours
 @author Aous Karoui
 @version 1.0
 **/

/** Constante pour le fichier de traduction (mails)*/
const translations = require('./../translations.json');
const Response = require('../models/Response');
/** Constante pour la configuration des requêtes sur COMPER */
const COMPER_REQUEST_CONFIG = require('../models/COMPER_REQUEST_CONFIG.json');
/** Constante pour le contenu des recommandations */
const COMPER_REQUEST_RECOM_STATEMENTS = require('./../models/COMPER_REQUEST_RECOM_STATEMENTS.json');
/** Les types d'intentions permettant de générer des recommandations */
const COMPER_OBJECTIVE_TYPES = require('../models/COMPER_OBJECTIVE_TYPES.json');
const config = require("../config.json");
const jwt = require("jsonwebtoken");

module.exports = (router, api) => {

    //======================================================================================
    //                                        GET
    //======================================================================================

    /**
     *    Accès à la liste des parcours d'une classe
     */
    router.get('/parcoursClasse', util.verifySession, (req, res) => {
        api.requestGETfromREST('getDegres').then(response => res.render('ajouterClasse', {degres: response.data.data}))
            .catch(() => res.redirect('404', {error: 'impossible d\'obtenir les degrés'}));
    });

    /**
     * Une page pour afficher la liste des parcours par classe
     */
    router.get('/parcoursClasse/:classId', util.verifySession, async (req, res) => {
        const parcours = (await api.getAllParcoursOfTarget('class', req.params.classId)).data;
        const classe = (await api.requestPOSTfromRESTWithID('getClasse', req.params.classId)).data;
        if (parcours.status === 'SUCCESS' && classe.status === 'SUCCESS') {
            return res.render('parcoursClasse', {
                parcours: parcours.data, classe: classe.data
            });
        } else {
            return res.render('404', {error: 'impossible de charger la page'});
        }
    });

    /**
     * La page sommaire du parcours d'un élève (vue enseignant)
     */
    router.get('/sommaireParcours/:studentId/:parcoursId', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        // on ré-affecte l'id de l'élève dans la session à cause du login (defaultStudentId)
        req.session.eleveID = req.params.studentId;
        const parcours = (await api.getSingleParcoursById(req.params.parcoursId)).data;
        const customParcoursSteps = (await api.getStudentCustomStepsOfParcours(req.params.studentId, req.params.parcoursId)).data.data.steps;
        const student = (await api.requestPOSTfromRESTWithID('getEleve', req.params.studentId)).data;
        const teacherView = true;
        setParcoursInSession(req, parcours);
        const {
            parcoursSteps,
            stepsDoneId,
            stepsDone,
            stepHistory,
            stepsNotDone,
            historique,
            listeExos
        } = await loadSteps(customParcoursSteps, parcours.data, req.params.studentId, teacherView, currentLang);

        if (parcours.status === 'SUCCESS' && student.status === 'SUCCESS') {
            return res.render(util.SOMMAIRE_PARCOURS_EJS, {
                parcours: parcours.data,
                parcoursSteps,
                listeExos,
                historique,
                stepsDone,
                stepsNotDone,
                stepHistory,
                stepsDoneId,
                teacherEmail: req.session.teacherEmail,
                eleve: student.data,
                currentPage: util.DATA_PAGE_TEACHER
            });
        } else {
            return res.render('404', {error: 'impossible de charger la page'});
        }
    });

    /**
     * La page de lancement d'un parcours pour un élève
     */
    router.get('/runParcours/:studentId/:parcoursId', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const parcours = (await api.getSingleParcoursById(req.params.parcoursId)).data;
        const customParcoursSteps = (await api.getStudentCustomStepsOfParcours(req.params.studentId, req.params.parcoursId)).data.data.steps;
        req.session.customParcoursSteps = customParcoursSteps;
        const eleve = (await api.requestPOSTfromRESTWithID('getEleve', req.params.studentId)).data;
        req.session.template = global.modeParcours;
        req.session.page = global.runParcours;

        const teacherView = false;
        setParcoursInSession(req, parcours);

        const {
            parcoursSteps,
            stepsDoneId,
            stepsDone,
            stepHistory,
            stepsNotDone,
            historique,
            listeExos
        } = await loadSteps(customParcoursSteps, parcours.data, req.params.studentId, teacherView, currentLang);

        if (eleve.status === 'SUCCESS') {
            return res.render(util.RUN_PARCOURS_EJS, {
                parcours: req.session.parcours,
                listeExos,
                parcoursSteps,
                historique,
                stepsDone,
                stepsNotDone,
                stepHistory,
                eleve: eleve.data,
                teacherEmail: req.session.teacherEmail,
                currentPage: util.RUN_PARCOURS_EJS
            });
        } else {
            return res.render('404', {error: 'impossible de charger la page'});
        }
    });

    /**
     * Une page pour afficher la liste des parcours par élève
     */
    router.get(`/${util.STUDENT_ALL_PARCOURS_EJS}/:studentId`, util.verifySession, async (req, res) => {
        const parcours = (await api.getAllParcoursOfTarget('student', req.params.studentId)).data;
        const eleve = (await api.requestPOSTfromRESTWithID('getEleve', req.params.studentId)).data;
        if (parcours.status === 'SUCCESS' && eleve.status === 'SUCCESS') {
            return res.render(util.STUDENT_ALL_PARCOURS_EJS, {
                teacherEmail: req.session.teacherEmail,
                parcours: parcours.data,
                eleve: eleve.data,
                currentPage: util.STUDENT_ALL_PARCOURS_EJS
            });
        } else {
            return res.render('404', {error: 'impossible de charger la page'});
        }
    });

    //======================================================================================
    //                                        POST
    //======================================================================================

    async function requestRecomWithFallback(session, fwId, initialRecomStatements, initialObjectives) {
        const FALLBACK_SEQUENCE = [
            COMPER_OBJECTIVE_TYPES.PRE_REQUIS,
            COMPER_OBJECTIVE_TYPES.SOUTIEN,
            COMPER_OBJECTIVE_TYPES.REVISION,
            COMPER_OBJECTIVE_TYPES.PERFECTIONNEMENT,
            COMPER_OBJECTIVE_TYPES.DECOUVERTE
        ];
        // Fonction utilitaire pour éviter la répétition de code
        const fetchRecommendations = async (statements, session) => {
            try {
                // Appelle makeComperRequest avec la logique de retry intégrée
                const recommandations = await makeComperRequest('generate', statements);
                return formatRecommendations(recommandations.data, session);
            } catch (error) {
                console.error('Failed to fetch recommendations:', error.message);
                throw error;
            }
        };
        // 1️⃣ Tentative initiale avec les recommandations de base
        let response = await fetchRecommendations(initialRecomStatements, session);
        if (!isEmptyResult(response)) return response;
        // 2️⃣ Fallback avec changement d'objectif
        for (const objectiveType of FALLBACK_SEQUENCE) {
            const newObjectives = [...initialObjectives]; // Clone léger
            newObjectives[0][1] = objectiveType;
            const newRecomStatements = await updateRecomStatements(
                session,
                fwId,
                COMPER_REQUEST_RECOM_STATEMENTS,
                newObjectives
            );
            response = await fetchRecommendations(newRecomStatements, session);
            if (!isEmptyResult(response)) break; // Sortir dès qu'on a une réponse valide
        }
        return response;
    }

    async function determineObjectives(fwId, session) {
        try {
            const studentObjectives = await getStudentObjectives(fwId, session);
            if (studentObjectives?.length > 0) return studentObjectives;
        } catch (e) {
            console.error("Erreur récupération objectifs étudiants", e);
        }
        try {
            const classObjectives = await getClassObjectives(fwId, session);
            if (classObjectives?.length > 0) return classObjectives;
        } catch (e) {
            console.error("Erreur récupération objectifs classe", e);
        }
        // Fallback par défaut
        return [
            COMPER_OBJECTIVE_TYPES.LECTURE_COMPREHENSION_COURT,
            COMPER_OBJECTIVE_TYPES.PRE_REQUIS
        ];
    }

    async function getStudentObjectives(fwId, session) {
        const studentObjectivesStatements = {
            fwid: fwId,
            username: "aousKaroui",
            user: `gameshub:${session.eleveID}`,
            role: "learner",
            username: `gameshub:${session.eleveID}`,
            platformGroupId: `gameshub:group_${session.classeID}_${fwId || 291}`
        };
        const response = await makeComperRequest('objectives', studentObjectivesStatements);
        console.log("Réponse studentObjectives :", response.data);
        return response.data;
    }

    async function getClassObjectives(fwId, session) {
        const classObjectivesStatements = {
            username: "aousKaroui",
            platformGroupId: `gameshub:group_${session.classeID}_${fwId || 291}`,
            fwid: fwId
        };
        const response = await makeComperRequest('classObjectives', classObjectivesStatements);
        console.log("Réponse classObjectives :", response.data);
        return response.data;
    }

    // services/apiService.js
    /**
     * Configure la requête COMPER avec une logique intégrée de retry
     * @param endpoint {string} endpoint correspondant à l'API COMPER
     * @param statements {object} données à envoyer lors de la requête
     * @param retries {number} nombre maximum de tentatives en cas d'échec
     * @param initialDelay {number} délai initial (en ms) entre les tentatives
     */
    async function makeComperRequest(endpoint, statements, retries = 4, initialDelay = 1000) {
        const jwtConfig = config.jwtCOMPER;
        const comperRequestConfig = structuredClone(COMPER_REQUEST_CONFIG);
        const token = await generateToken(statements, jwtConfig);
        comperRequestConfig.url += endpoint;
        comperRequestConfig.headers.Authorization += token;
        console.log("Token : " + endpoint + " : " + token);

        let delay = initialDelay;
        let lastError;
        for (let attempt = 1; attempt <= retries; attempt++) {
            try {
                // Tentative de requête avec axios
                return await axios.request(comperRequestConfig);
            } catch (error) {
                lastError = error;
                console.error(`Attempt ${attempt} failed: ${error.message}`);
                // Si on a atteint le nombre maximal de tentatives, on lève une erreur
                if (attempt === retries) {
                    throw lastError;
                }
                // Attente avant la prochaine tentative, avec un délai croissant
                await new Promise(resolve => setTimeout(resolve, delay));
                delay *= 2; // Augmentation exponentielle du délai
            }
        }
    }


    // utils/authHelpers.js
    async function generateToken(payload, config) {
        return jwt.sign(payload, config.secret, {
            algorithm: config.algorithm,
            expiresIn: config.exp
        });
    }

    // 2. Helper function pour vérifier les résultats vides
    const isEmptyResult = (result) => {
        return !result || result.length === 0 || result === 0;
    };

    // services/recommendationService.js
    /**
     * Retourne le json des données/statements de la requête mis à jour
     * @param session {object} session actuelle
     * @param fwId {number} identifiant du framewrok COMPER
     * @param recomData {object} statements bruts de la requête
     */
    async function updateRecomStatements(session, fwId, recomData, objectives) {
        const clonedRecomData = structuredClone(recomData);
        clonedRecomData.user += session.eleveID;
        clonedRecomData.fwid = fwId;
        clonedRecomData.username += session.eleveID;
        clonedRecomData.platformGroupId += `${session.classeID}_${fwId || 291}`;
        if (objectives && Object.keys(objectives).length) {
            // Cloner clonedRecomData pour éviter la mutation accidentelle
            let updatedRecomData = {...clonedRecomData};
            // Remplacer les objectifs
            updatedRecomData.objectives = [...objectives]; // Copie indépendante
        }
        return clonedRecomData;
    }

    // utils/responseUtils.js
    function formatRecommendations(recommendations, session) {
        // Extraction sécurisée de l'ID avec gestion des URLs malformées
        const extractId = (url) => url?.split('/')?.filter(Boolean)?.pop();
        // Normalisation de customParcoursSteps
        let customParcoursSteps = [];
        try {
            const steps = session.customParcoursSteps;
            customParcoursSteps = typeof steps === 'string'
                ? (steps.trim() ? JSON.parse(steps) : [])
                : Array.isArray(steps) ? steps : [];
        } catch (error) {
            console.error("Erreur de parsing des étapes, réinitialisation :", error);
            customParcoursSteps = [];
        }
        // Création d'un Set pour des contrôles optimisés
        const existingIds = new Set(
            customParcoursSteps
                .map(step => step?.id?.toString())
                .filter(Boolean)
        );
        // Conversion unifiée en tableau
        const recommendationList = Array.isArray(recommendations)
            ? recommendations
            : Object.values(recommendations);
        // Filtrage et traitement des résultats
        return recommendationList
            .filter(({ location }) => {
                const id = extractId(location);
                return id && !existingIds.has(id);
            })
            .slice(0, 3)
            .map(({ location }) => extractId(location));
    }

    /**
     * récupérer recommandations de Profile-Engine
     */
    router.post('/requestRecommendations', util.verifySession, async (req, res) => {
        try {
            const {fwId} = req.body;
            const session = req.session;
            const objectives = await determineObjectives(fwId, session);
            const recomStatements = await updateRecomStatements(
                session,
                fwId,
                COMPER_REQUEST_RECOM_STATEMENTS,
                objectives
            );
            const response = await requestRecomWithFallback(session, fwId, recomStatements, objectives);
            res.status(200).json(response);
        } catch (e) {
            console.error("Erreur globale :", e);
            res.status(500).json({
                error: "Erreur interne du serveur",
                details: e.message
            });
        }
    });

    const normalizeString = function (string) {
        // enlève les espaces
        let result = string.replace(/ /g, '');
        result = result.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
        return result;
    };
    /**
     * récupérer recommandations de Profile-Engine
     */
    router.post('/checkAdaptiveLearning', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        if (util.AL_PARCOURS_IDS.includes(req.session.parcours.id)) {
            let oldCustomSteps = [];
            const newStepJson = {};
            newStepJson.number = 1;
            // on récupère l'ancien parcours s'il existe
            if (req.session.customParcoursSteps) {
                oldCustomSteps = req.session.customParcoursSteps;
                oldCustomSteps = JSON.parse(oldCustomSteps);
                newStepJson.number = oldCustomSteps.length + 1;
            }
            // On récupère les données de l'exercice en cours
            const currentExo = (await api.getExerciceById(req.session.forcedExerciceId)).data.data;
            try {
                // On crée une nouvelle étape en format json
                newStepJson.niveau = currentExo.description;
                const parsedGameName = JSON.parse(currentExo.gameName);
                newStepJson.fullName = parsedGameName.fr;
                newStepJson.shortName = normalizeString(JSON.stringify(parsedGameName.fr));
                newStepJson.gameId = currentExo.gameId;
                newStepJson.type = 'next';
                newStepJson.defaultRemediationId = '';
                // FIXME à harmoniser avec addNewStep de ui_parcours (pour id et evaluation)
                newStepJson.id = currentExo.id;
                newStepJson.studentScore = req.body.evaluation;
                oldCustomSteps.push(newStepJson);
                const response = await api.updateCustomStepsJson(req.session.eleveID, req.session.parcours.id, JSON.stringify(oldCustomSteps));
                res.status(200).json(response);
            } catch (error) {
                console.error("Erreur lors de l'analyse JSON :", error.message);
            }
        }
    });

    /**
     * Insertion d'un nouveau parcours dans la bdd
     */
    router.post('/createParcours', util.verifySession, async (req, res) => {

        const {nomParcours, targetSkills} = req.body;
        const currentLang = util.getLang(res);

        const data = (await api.createParcours(
                req.session.teacherID,
                nomParcours,
                targetSkills)
        ).data.data;
        return new Response('success', 200, translations[currentLang].parcours_created, data).send(res);
    });

    /**
     * Pour vérifier si un parcours existe pour une classe donnée
     */
    router.post('/checkParcoursFor', util.verifySession, async (req, res) => {
        const {target, targetId} = req.body;
        const parcoursId = req.session.parcours.id;
        const data = (await api.checkParcoursFor(target, targetId, parcoursId)).data;
        if (data.status === 'SUCCESS') {
            // return new Response('info', 200, 'info').send(res);
            new Response('success', 200, '', false).send(res);
        } else {
            new Response('error', 500).send(res);
        }
    });

    /**
     * Création/m.à.j d'un parcours dans la base (en fonction du requestURL)
     */
    router.post('/saveParcours', util.verifySession, async (req, res) => {
        // On récupère les parcours depuis la base à chaque fois pour être à jour avec les dernières insertions
        let parcoursPrives = api.requestPOSTfromRESTWithID('getParcours', req.session.teacherID);
        parcoursPrives = (await parcoursPrives).data;
        // variable pour indiquer si le nom du parcours existe déjà dans la base
        let nomExiste = false;
        const {competences, stepsJson, nomParcours, licence, degre, description, requestURL} = req.body;
        const currentLang = util.getLang(res);

        // Si on part de la page designParcours ce sera le parcoursMetaData de la session sinon le paramètre sera dans competences
        let metaData;
        if (competences) metaData = competences; else metaData = req.session.parcoursMetaData;

        // On vérifie donc si le nom existe déjà
        for (const parcours of parcoursPrives.data) {
            if (parcours.nom === nomParcours) {
                nomExiste = true;
                break;
            }
        }
        // saveParcours = nouvelle création (ce n'est donc pas un update)
        if ((requestURL === 'saveParcours' && !nomExiste) || (requestURL === 'updateParcours' && nomExiste)) {
            api.saveParcours(req.session.teacherID, nomParcours, licence, description, degre, JSON.stringify(metaData), JSON.stringify(stepsJson), requestURL).then(() => {
                return new Response('success', 200, translations[currentLang].parcours_saved, true).send(res);
            }).catch((e) => {
                console.error(e.message);
                return new Response('error', 500, e.message).send(res);
            });
        } else {
            if (requestURL === 'saveParcours') {
                return new Response('error', 400, translations[currentLang].parcours_already_exists, true).send(res);
            } else if (requestURL === 'updateParcours') {
                return new Response('error', 400, translations[currentLang].parcours_does_not_exist, true).send(res);
            }
        }
    });

    /**
     * Sauvegarde d'un parcours pour une classe ou un élève
     */
    router.post('/saveParcoursTo', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {target, targetId, clickedName} = req.body;
        const parcoursData = (await api.getSingleParcoursById(req.session.parcoursID)).data.data;

        api.saveParcoursTo(target, targetId, req.session.parcours.id, parcoursData.steps).then(() => {
            return new Response('success', 200, translations[currentLang].parcours_saved_to + clickedName).send(res);
        }).catch((e) => {
            console.error(e.message);
            return new Response('error', 500, e.message).send(res);
        });
    });

    /**
     * M.à.j des MetaData d'un parcours (page parcoursDesign)
     */
    router.post('/updateParcoursMetaData', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {title, description, licence, degre, tag, parcoursId} = req.body;
        const data = (await api.updateParcoursMetaData(
                title,
                description,
                licence,
                degre,
                tag,
                parcoursId)
        ).data.data;
        return new Response('success', 200, translations[currentLang].parcours_saved, data).send(res);
    });

    /**
     * Modifie le statut d'un parcours (privé / en attente / public)
     */
    router.post('/updateParcoursStatus', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {id, status, parcoursNumber} = req.body;
        let canDelete = true;
        // On récupère les steps du parcours en question
        if (req.body.parcoursNumber) {
            const parcoursSteps = req.session.parcoursPrives[parcoursNumber].steps;
            // On check si les steps contiennent un exo unplugged
            for (const step of JSON.parse(parcoursSteps)) {
                if (step.gameId === '16' || step.gameId === '18') {
                    canDelete = false;
                    break;
                }
            }
        }
        if (canDelete) {
            api.updateParcoursStatus(id, status).then(() => {
                return new Response('success', 200, 'Opération réalisée avec succès', true).send(res);
            }).catch((e) => {
                console.error(e.message);
                return new Response('error', 500, e.message).send(res);
            });
        } else return new Response('error', 500, 'Parcours non partagé').send(res);
    });

    /**
     * Modifie le json de l'étape pour la valider (ou la ré-initialiser)
     */
    router.post('/updateStepsJson', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const parcoursId = req.session.parcours.id;
        const {stepsJson} = req.body;
        api.updateStepsJson(parcoursId, JSON.stringify(stepsJson)).then(() => {
            return new Response('success', 200, 'Opération réalisée avec succès', true).send(res);
        }).catch((e) => {
            console.error(e.message);
            return new Response('error', 500, e.message).send(res);
        });
    });


    /**
     * Pour vérifier si un parcours existe pour une classe donnée
     */
    router.post('/getSingleParcoursByUserAndName', util.verifySession, async (req, res) => {
        const {nomParcours} = req.body;
        const data = (await api.getSingleParcoursByUserAndName(req.session.teacherID, nomParcours)).data;
        if (data.status === 'SUCCESS') {
            req.session.parcours.id = data.data.id;
            new Response('success', 200, '', false).send(res);
        } else {
            new Response('error', 500).send(res);
        }
    });

    /**
     * Suppression d'un parcours pour une classe ou un élève
     */
    router.post('/deleteParcoursFrom', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {target, targetId, clickedName} = req.body;
        api.deleteParcoursFrom(target, targetId, req.session.parcours.id).then(() => {
            return new Response('success', 200, translations[currentLang].parcours_deleted_from + clickedName).send(res);
        }).catch((e) => {
            console.error(e.message);
            return new Response('error', 500, e.message).send(res);
        });
    });

    /**
     * Suppression d'un parcours pour un professeur
     */
    router.post('/deleteParcoursWithId', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {parcoursId} = req.body;
        api.deleteParcoursWithId(parcoursId).then(() => {
            return new Response('success', 200, translations[currentLang].parcours_deleted, true).send(res);
        }).catch((e) => {
            console.error(e.message);
            return new Response('error', 500, e.message).send(res);
        });
    });

    /**
     * Réinitialise un parcours pour un élève en supprimant les historiques de ses exercices dans le parcours
     */
    router.post('/resetParcoursForStudent', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const {studentId, parcoursId} = req.body;
        const data = (await api.resetParcoursForStudent(studentId, parcoursId)).data;
        if (data.status === 'SUCCESS') {
            new Response('success', 200, translations[currentLang].resetParcoursSuccess, false, {id: data.data}).send(res);
        } else {
            new Response('error', 500, data, true).send(res);
        }
    });

    /**
     * Vérifie combien de fois un niveau a été fait dans un parcours pour un élève donné
     */
    router.post('/checkStepDoneForStudent', util.verifySession, async (req, res) => {
        const currentLang = util.getLang(res);
        const studentId = req.session.eleveID;
        const parcoursId = req.session.parcours.id;
        const exoId = req.session.forcedExerciceId;
        const data = (await api.checkStepDoneForStudent(studentId, parcoursId, parseInt(exoId))).data;
        if (data.status === 'SUCCESS') {
            console.log(data);
            new Response('success', 200, '', false, {count: data.data}).send(res);
        } else {
            new Response('error', 500, data, true).send(res);
        }
    });


//==================================================================
//====  Fonctions diverses utilisées par les requêtes ci-dessus ====
//==================================================================

    /**
     * Ré-initialise le parcours dans la session
     */
    function setParcoursInSession(req, parcours) {
        req.session.parcours = {};
        req.session.parcours = parcours.data;
    }

    /**
     * Récupère la liste des exos d'un parcours
     */
    async function getExercisesList(parcoursSteps, currentLang) {
        const stepsId = parcoursSteps.map(element => element.id);
        const listeExos = (await api.getParcoursExercices(stepsId)).data.data;
        for (const exo of listeExos) {
            if (exo.gameName) {
                exo.gameName = JSON.parse(exo.gameName)[currentLang];
            }
        }
        return listeExos;
    }

    /**
     * Charge la liste des étapes du parcours
     * @param customParcoursSteps {json} les étapes de parcours personnalisés pour chaque élève
     * @param parcours {json} le parcours initial (initié par l'enseignant)
     * @param studentId {number} l'identifiant de l'élève
     * @param teacherView {boolean} le type de vue (si vrai enseignant sinon vue élève)
     * @param currentLang {string} la langue utilisée
     */
    async function loadSteps(customParcoursSteps, parcours, studentId, teacherView, currentLang) {
        try {
            // 1. Validation des entrées
            const parseSteps = (jsonString) => {
                if (!jsonString) return [];
                try {
                    const parsed = JSON.parse(jsonString);
                    if (!Array.isArray(parsed)) throw new Error("Invalid steps format");
                    return parsed;
                } catch (e) {
                    throw new Error(`Invalid JSON format: ${e.message}`);
                }
            };
            // 2. Récupération des étapes avec validation
            const parcoursSteps = customParcoursSteps !== undefined ?
                parseSteps(customParcoursSteps) :
                parseSteps(parcours.steps);

            // 3. Copie des étapes pour manipulation
            const stepsNotDone = [...parcoursSteps];

            const stats = (await api.getStudentParcoursStats(studentId, parcours.id)).data;
            const historique = stats.data.historique || [];

            const stepsDoneId = [], stepsDone = [], stepHistory = [];

            for (const hist of historique) {
                let i = 0;
                while (stepsNotDone.length > 0 && i < stepsNotDone.length) {
                    if (hist.exerciceId == stepsNotDone[i].id) {
                        stepsDoneId.push(parseInt(stepsNotDone[i].id));
                        stepsDone.push(stepsNotDone[i]);
                        stepHistory.push(hist);
                        stepsNotDone.splice(stepsNotDone.indexOf(stepsNotDone[i]), 1);
                        if (!teacherView)
                            handleRemediation(hist, stepsNotDone, i);
                    } else i++;
                }
            }
            const listeExos = await getExercisesList(parcoursSteps, currentLang);

            return {parcoursSteps, stepsDoneId, stepsDone, stepHistory, stepsNotDone, listeExos};
        } catch (e) {
            // Journalisation sécurisée
            console.error("Erreur complète :", e);
            // Si l'erreur vient d'une réponse API (ex: Axios)
            if (e.response)
                console.error("Détails de l'erreur API :", e.response.data);
            // Si c'est une erreur de validation JSON
            else if (e.message.includes("JSON"))
                console.error("Erreur de parsing JSON :", e.message);
            // Toutes les autres erreurs
            else
                console.error("Erreur inattendue :", e.message, e.stack);
            throw new Error("Échec du chargement des étapes");
        }
    }

    /**
     * Gère l'accès aux remédiations dans le parcours
     * @param {Object} hist - L'historique (contenant par exemple hist.evaluation)
     * @param {Array} stepsNotDone - Le tableau des étapes restantes
     * @param {number} i - L'indice de l'étape à vérifier dans stepsNotDone
     */
    function handleRemediation(hist, stepsNotDone, i) {
        // Vérifier que l'indice i est valide
        if (i < 0 || i >= stepsNotDone.length) {
            console.warn("Indice invalide dans handleRemediation :", i);
            return;
        }
        // Si l'évaluation est égale à 2 et que l'étape à l'indice i est une remédiation, on la retire
        if (hist.evaluation === 2 && stepsNotDone[i]?.type === 'remediation') {
            stepsNotDone.splice(i, 1);
        }
    }

};